SELECT
  --   Business Key                                                                                       
  E_T.OptionNo AS ItemLedgerEntryTypeCode
  --  Attributes
  , COALESCE(NULLIF(E_T.OptionValue, ''), 'N/A') AS TypeDescription
  , E_T.DataConnectionId AS DataConnectionID
FROM help.Enumerations AS E_T
WHERE E_T.TableName = 'Item Ledger Entry'
  AND E_T.ColumnName = 'Entry Type'
