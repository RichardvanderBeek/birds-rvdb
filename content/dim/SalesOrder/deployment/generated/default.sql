SELECT
  -- System information
  SH.stage_id AS StageID
  , SH.company_id AS CompanyID
  , SH.data_connection_id AS DataConnectionID
  -- Business Key                    
  , SH.No_ AS DocumentCode
  , SH.[Document Type] AS DocumentTypeCode
  -- Attribute
  , COALESCE(e_dt.OptionValue, 'N/A') AS DocumentTypeDescription
  , SH.[Status] AS SalesStatusCode
  , COALESCE(e_st.OptionValue, 'N/A') AS SalesStatusDescription
FROM stage_nav.[Sales Header] AS SH
LEFT JOIN help.Enumerations AS e_dt
  ON e_dt.OptionNo = SH.[Document Type]
    AND e_dt.ColumnName = 'Document Type'
    AND e_dt.TableName = 'Sales Header'
    AND e_dt.dataconnectionid = SH.data_connection_id
LEFT JOIN help.Enumerations AS e_st
  ON e_st.OptionNo = SH.[Status]
    AND e_st.ColumnName = 'Status'
    AND e_st.TableName = 'Sales Header'
    AND e_st.dataconnectionid = SH.data_connection_id
