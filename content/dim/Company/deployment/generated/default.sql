SELECT
  -- System information
  Co.Company_Id AS CompanyID
  , Co.data_connection_id AS DataConnectionID
  -- Business key
  , Co.Code AS CompanyCode
  , Co.Code AS CompanyName
FROM meta.companies AS Co
