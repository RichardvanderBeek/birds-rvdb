SELECT
  -- system information
  BU.stage_id AS StageID
  , BU.company_id AS CompanyID
  , BU.data_connection_id AS DataConnectionID
  -- business key
  , BU.Code AS BusinessUnitCode
  --  attributes                        
  , COALESCE(NULLIF(BU.NAME, ''), 'N/A') AS BusinessUnitName
  , COALESCE(NULLIF(BU.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(BU.NAME, ''), 'N/A') AS BusinessUnitCodeName
FROM stage_nav.[Business Unit] AS BU
