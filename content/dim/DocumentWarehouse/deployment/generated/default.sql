WITH DocumentWarehouse
AS (
  SELECT PWRH.company_id AS CompanyID
    , PWRH.data_connection_id AS DataConnectionID
    --business key
    , PWRH.[No_] AS WarehouseDocumentCode
    , 1 AS WarehouseDocumentTypeCode
    --attributes
    , PWRH.[Posting Date] AS PostingDate
    , PWRH.[Vendor Shipment No_] AS VendorShipmentNo
    , PWRH.[Assignment Date] AS AssignmentDate
    , PWRH.[Assignment Time] AS AssignmentTime
  FROM stage_nav.[Posted Whse_ Receipt Header] PWRH --Posted Warehouse Receipt Header
  GROUP BY PWRH.company_id
    , PWRH.data_connection_id
    , PWRH.[No_]
    , PWRH.[Posting Date]
    , PWRH.[Vendor Shipment No_]
    , PWRH.[Assignment Date]
    , PWRH.[Assignment Time]
  
  UNION ALL
  
  SELECT PWSH.company_id AS CompanyID
    , PWSH.data_connection_id AS DataConnectionID
    --business key
    , PWSH.[No_] AS WarehouseDocumentCode
    , 2 AS WarehouseDocumentTypeCode
    --attributes
    , PWSH.[Posting Date] AS PostingDate
    , NULL AS VendorShipmentNo
    , PWSH.[Assignment Date] AS AssignmentDate
    , PWSH.[Assignment Time] AS AssignmentTime
  FROM stage_nav.[Posted Whse_ Shipment Header] PWSH --Posted Warehouse Shipment Header
  GROUP BY PWSH.company_id
    , PWSH.data_connection_id
    , PWSH.[No_]
    , PWSH.[Posting Date]
    , PWSH.[Assignment Date]
    , PWSH.[Assignment Time]
  
  UNION ALL
  
  SELECT RWAH.company_id AS CompanyID
    , RWAH.data_connection_id AS DataConnectionID
    --business key
    , RWAH.[No_] AS WarehouseDocumentCode
    , 2 + RWAH.[Type] AS WarehouseDocumentTypeCode
    --attributes
    , RWAH.[Registering Date] AS PostingDate
    , NULL AS VendorShipmentNo
    , RWAH.[Assignment Date] AS AssignmentDate
    , RWAH.[Assignment Time] AS AssignmentTime
  FROM stage_nav.[Registered Whse_ Activity Hdr_] RWAH --Registered Warehouse Activity Header
  GROUP BY RWAH.company_id
    , RWAH.data_connection_id
    , RWAH.[No_]
    , RWAH.[Type]
    , RWAH.[Registering Date]
    , RWAH.[Assignment Date]
    , RWAH.[Assignment Time]
  )
SELECT DW.CompanyID AS CompanyID
  , DW.DataConnectionID AS DataConnectionID
  --business key
  , DW.WarehouseDocumentCode AS WarehouseDocumentCode
  , DW.WarehouseDocumentTypeCode AS WarehouseDocumentTypeCode
  --attributes
  , COALESCE(NULLIF(e_dt.OptionValue, ''), 'N/A') AS DocumentTypeDescription
  , COALESCE(NULLIF(DW.PostingDate, '1753-01-01 00:00:00.000'), '1900-01-01 00:00:00.000') AS PostingDate
  , COALESCE(NULLIF(DW.VendorShipmentNo, ''), 'N/A') AS VendorShipmentNo
  , COALESCE(NULLIF(DW.AssignmentDate, '1753-01-01 00:00:00.000'), '1900-01-01 00:00:00.000') AS AssignmentDate
  , COALESCE(NULLIF(DW.AssignmentTime, '1753-01-01 00:00:00.000'), '1900-01-01 00:00:00.000') AS AssignmentTime
FROM DocumentWarehouse DW
LEFT JOIN help.Enumerations AS e_dt
  ON e_dt.OptionNo = DW.WarehouseDocumentTypeCode
    AND e_dt.TableName = 'Warehouse Entry'
    AND e_dt.ColumnName = 'Whse. Document Type'
    AND e_dt.DataConnectionId = DW.DataConnectionID
