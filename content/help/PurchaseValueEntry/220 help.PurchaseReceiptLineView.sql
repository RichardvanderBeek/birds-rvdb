/***********************************************************************************
Functionality:  This SQL script creates the help.PurchaseReceiptLineView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to PurchaseLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.PurchaseReceiptLineView', @type = N'V' ;
GO
CREATE VIEW help.PurchaseReceiptLineView
AS
  SELECT
    -- system information
                    PRL.stage_id                                     AS StageID
                  , PRL.company_id                                   AS CompanyID
                  , PRL.data_connection_id                           AS DataConnectionID
                  -- business key
                  , VE.[Entry No_]                                   AS EntryNo
                  , VE.[Document Type]                               AS DocumentType
                  , VE.[Document No_]                                AS DocumentNo
                  , VE.[Posting Date]                                AS PostingDate
                  , COALESCE(PRH.[Posting Date], PRL.[Posting Date]) AS DocumentPostingDate
                  , PRL.[Buy-from Vendor No_]                        AS BuyFromVendorNo
                  , PRL.[Pay-to Vendor No_]                          AS PayToVendorNo
                  , COALESCE(PRH.[Currency Factor], 1)               AS CurrencyFactor
                  , PRH.[Currency Code]                              AS CurrencyCode
                  , PRL.[Type]                                       AS [Type]
    FROM            stage_nav.[Purch_ Rcpt_ Line]   AS PRL
    LEFT OUTER JOIN stage_nav.[Purch_ Rcpt_ Header] AS PRH ON PRL.[Document No_]         = PRH.[No_]
                                                          AND PRL.company_id             = PRH.company_id
                                                          AND PRL.data_connection_id     = PRH.data_connection_id
   INNER JOIN       stage_nav.[Value Entry]         AS VE ON VE.[Document Type]          = 5 --Receipt
                                                         AND PRL.[Document No_]          = VE.[Document No_]
                                                         AND PRL.[Line No_]              = VE.[Document Line No_]
                                                         AND VE.[Item Ledger Entry Type] = 0 --Purchase  
                                                         AND PRL.company_id              = VE.company_id
                                                         AND PRL.data_connection_id      = VE.data_connection_id
   WHERE            PRL.execution_flag <> 'D'
     AND            PRH.execution_flag            <> 'D'
     AND            VE.execution_flag             <> 'D' ;
GO
