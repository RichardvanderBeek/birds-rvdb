SELECT
  -- system information
  FA.stage_id AS StageID
  , FA.company_id AS CompanyID
  , FA.data_connection_id AS DataConnectionID
  -- business key
  , FA.No_ AS FixedAssetCode
  --  attributes                        
  , COALESCE(NULLIF(FA.Description, ''), 'N/A') AS FixedAssetDescription
  , COALESCE(NULLIF(FA.No_, ''), 'N/A') + ' - ' + COALESCE(NULLIF(FA.Description, ''), 'N/A') AS FixedAssetCodeDescription
  , COALESCE(NULLIF(FA.[Fa Class Code], ''), 'N/A') AS FixedAssetClassCode
  , COALESCE(NULLIF(FAC.NAME, ''), 'N/A') AS FixedAssetClassDescription
  , COALESCE(NULLIF(FA.[Fa Class Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(FAC.NAME, ''), 'N/A') AS FixedAssetClassCodeDescription
  , COALESCE(NULLIF(FA.[Fa Subclass Code], ''), 'N/A') AS FixedAssetSubclassCode
  , COALESCE(NULLIF(FAS.NAME, ''), 'N/A') AS FixedAssetSubclassDescription
  , COALESCE(NULLIF(FA.[Fa Subclass Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(FAS.NAME, ''), 'N/A') AS FixedAssetSubclassCodeDescription
  , FA.[Budgeted Asset] AS FixedBudgetedAssetCode
  , COALESCE(e_a.OptionValue, 'N/A') AS FixedBudgetedAssetDescription
  , FA.Blocked AS FixedAssetBlockedCode
  , COALESCE(e_b.OptionValue, 'N/A') AS FixedAssetBlockedDescription
  , FA.Inactive AS FixedAssetInactiveCode
  , COALESCE(e_i.OptionValue, 'N/A') AS FixedAssetInactiveDescription
  , COALESCE(NULLIF(FA.[Vendor No_], ''), 'N/A') AS VendorCode
  , COALESCE(NULLIF(V.NAME, ''), 'N/A') AS VendorName
  , COALESCE(NULLIF(FA.[Vendor No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(V.NAME, ''), 'N/A') AS VendorCodeName
  , COALESCE(NULLIF(FA.[Maintenance Vendor No_], ''), 'N/A') AS MaintenanceVendorCode
  , COALESCE(NULLIF(V.NAME, ''), 'N/A') AS MaintenanceVendorName
  , COALESCE(NULLIF(FA.[Maintenance Vendor No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(V.NAME, ''), 'N/A') AS MaintenanceVendorCodeName
  , COALESCE(NULLIF(FA.[Responsible Employee], ''), 'N/A') AS ResponsibleEmployeeCode
  , COALESCE(NULLIF(COALESCE(NULLIF(E.[First Name], '') + ' ', '') + COALESCE(NULLIF(E.[Middle Name], '') + ' ', '') + COALESCE(NULLIF(E.[Last Name], ''), ''), ''), 'N/A') AS ResponsibleEmployeeName
  , COALESCE(NULLIF(FA.[Responsible Employee], ''), 'N/A') + ' - ' + COALESCE(NULLIF(COALESCE(NULLIF(E.[First Name], '') + ' ', '') + COALESCE(NULLIF(E.[Middle Name], '') + ' ', '') + COALESCE(NULLIF(E.[Last Name], ''), ''), ''), 'N/A') AS ResponsibleEmployeeCodeName
  , COALESCE(NULLIF(FA.[Serial No_], ''), 'N/A') AS SerialCode
  , COALESCE(NULLIF(FA.[Location Code], ''), 'N/A') AS FixedAssetLocationCode
  , COALESCE(NULLIF(L.NAME, ''), 'N/A') AS FixedAssetLocationName
  , COALESCE(NULLIF(FA.[Location Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(L.NAME, ''), 'N/A') AS FixedAssetLocationCodeName
FROM stage_nav.[Fixed Asset] AS FA
LEFT JOIN help.Enumerations AS e_a
  ON e_a.OptionNo = FA.[Budgeted Asset]
    AND e_a.ColumnName = 'Budgeted Asset'
    AND e_a.TableName = 'Fixed Asset'
    AND e_a.dataconnectionid = FA.data_connection_id
LEFT JOIN help.Enumerations AS e_b
  ON e_b.OptionNo = FA.Blocked
    AND e_b.ColumnName = 'Blocked'
    AND e_b.TableName = 'Fixed Asset'
    AND e_b.dataconnectionid = FA.data_connection_id
LEFT JOIN help.Enumerations AS e_i
  ON e_i.OptionNo = FA.Inactive
    AND e_i.ColumnName = 'Inactive'
    AND e_i.TableName = 'Fixed Asset'
    AND e_i.dataconnectionid = FA.data_connection_id
LEFT JOIN stage_nav.[FA Class] AS FAC
  ON FAC.Code = FA.[Fa Class Code]
    AND FAC.company_id = FA.company_id
    AND FAC.data_connection_id = FA.data_connection_id
LEFT JOIN stage_nav.[FA Subclass] AS FAS
  ON FAS.Code = FA.[Fa Subclass Code]
    AND FAS.company_id = FA.company_id
    AND FAS.data_connection_id = FA.data_connection_id
LEFT JOIN stage_nav.Vendor AS V
  ON V.[No_] = FA.[Vendor No_]
    AND V.company_id = FA.company_id
    AND V.data_connection_id = FA.data_connection_id
LEFT JOIN stage_nav.Employee AS E
  ON E.No_ = FA.[Responsible Employee]
    AND E.company_id = FA.company_id
    AND E.data_connection_id = FA.data_connection_id
LEFT JOIN stage_nav.Location AS L
  ON L.Code = FA.[Location Code]
    AND L.company_id = FA.company_id
    AND L.data_connection_id = FA.data_connection_id
LEFT JOIN stage_nav.[FA Location] AS FAL
  ON FAL.[Code] = FA.[Fa Location Code]
    AND FAL.company_id = FA.company_id
    AND FAL.data_connection_id = FA.data_connection_id
