/****************************************************************************************************
  Functionality: Returns the max level of the hierachy with a minimum of 4.
    

  Created by:    JvL	Date:	2019/02/26
  Date 		Changed by 	Ticket/Change 	Description
*****************************************************************************************************/

EXEC dbo.drop_object @object = N'help.GetJobTaskHierachiesMaxLevel', @type = N'F' ;
GO
CREATE FUNCTION help.GetJobTaskHierachiesMaxLevel
()
RETURNS INT
BEGIN
  DECLARE @max_level INT = 4 ;

  IF EXISTS (SELECT * FROM stage_nav.[Job Task])
  BEGIN
    SELECT @max_level = CASE WHEN MAX(Indentation) > 4 THEN MAX(Indentation) ELSE @max_level END
      FROM stage_nav.[Job Task] ;
  END ;
  RETURN @max_level ;
END ;
