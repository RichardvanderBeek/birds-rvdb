SELECT
  -- system information
  S.stage_id AS StageID
  , S.company_id AS CompanyID
  , S.data_connection_id AS DataConnectionID
  -- business key
  , S.Code AS ScrapCode
  --  attributes
  , S.Description AS ScrapDescription
  , S.Code + ISNULL(' - ' + NULLIF(S.Description, ''), '') AS ScrapCodeDescription
FROM stage_nav.[Scrap] AS S
