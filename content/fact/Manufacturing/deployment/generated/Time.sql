WITH cte_productiontimes
AS (
  SELECT VE.stage_id
    , VE.execution_flag
    , VE.execution_timestamp
    , VE.company_id
    , VE.data_connection_id
    , VE.[Capacity Ledger Entry No_]
    , VE.[Location Code]
  FROM stage_nav.[Value Entry] AS VE
  GROUP BY VE.stage_id
    , VE.execution_flag
    , VE.execution_timestamp
    , VE.company_id
    , VE.data_connection_id
    , VE.[Capacity Ledger Entry No_]
    , VE.[Location Code]
  )
SELECT
  -- system information
  CLE.stage_id AS StageID
  , CLE.Company_Id AS CompanyID
  , CLE.data_connection_id AS DataConnectionID
  , 3 AS ManufacturingType
  -- dimensions
  , CLE.[Entry No_] AS EntryNo
  , 5 AS EntryType
  , CLE.[Posting Date] AS PostingDate
  , CLE.[Order No_] AS ProductionOrderCode
  , CLE.[Item No_] AS ItemCode
  , CLE.[Item No_] AS OutputItemCode
  , CAST(NULL AS NVARCHAR) AS WorkCenterCode
  , CLE.[Type] AS SourceType
  , CLE.[Scrap Code] AS ScrapCode
  , CLE.[Stop Code] AS StopCode
  , CTE.[Location Code] AS LocationCode
  , CLE.Subcontracting AS SubcontractingCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , CAST(NULL AS DECIMAL(38, 20)) AS ActualOutputQuantity
  , CLE.Quantity AS ActualConsumptionQuantity
  , CLE.Quantity AS ActualTotalQuantity
  , CAST(NULL AS DECIMAL(38, 20)) AS ActualOutputCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ActualConsumptionCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ActualTotalCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ActualOverheadCost
  , CLE.[Scrap Quantity] AS ScrapQuantity
  , CLE.[Setup Time] AS SetupTime
  , CLE.[Run Time] AS RunTime
  , CLE.[Stop Time] AS StopTime
FROM stage_nav.[Capacity Ledger Entry] AS CLE
LEFT JOIN cte_productiontimes AS CTE
  ON CLE.[Entry No_] = CTE.[Capacity Ledger Entry No_]
    AND CLE.company_id = CTE.company_id
    AND CLE.data_connection_id = CTE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON CLE.[Dimension Set ID] = FD.DimensionSetNo
    AND CLE.company_id = FD.CompanyID
    AND CLE.data_connection_id = FD.DataConnectionID
WHERE CLE.[Entry No_] <> 0
