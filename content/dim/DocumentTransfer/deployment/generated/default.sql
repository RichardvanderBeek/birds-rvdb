SELECT
  -- System information
  DS.StageID AS StageID
  , DS.CompanyID AS CompanyID
  , DS.DataConnectionID AS DataConnectionID
  -- Business Key                    
  , DS.DocumentNo AS DocumentTransferCode
  , DS.[DocumentType] AS DocumentTypeCode
  -- Attribute
  , COALESCE(e_dt.OptionValue, 'N/A') AS DocumentTypeDescription
FROM (
  SELECT DISTINCT TSH.stage_id AS StageID
    , TSH.company_id AS CompanyID
    , TSH.data_connection_id AS DataConnectionID
    , TSH.[Posting Date] AS PostingDate
    , 9 AS DocumentType --Transfer Shipment
    , TSH.[No_] AS DocumentNo
  FROM stage_nav.[Transfer Shipment Header] AS TSH
  
  UNION ALL
  
  SELECT DISTINCT TRH.stage_id AS StageID
    , TRH.company_id AS CompanyID
    , TRH.data_connection_id AS DataConnectionID
    , TRH.[Posting Date] AS PostingDate
    , 10 AS DocumentType --Transfer Receipt
    , TRH.[No_] AS DocumentNo
  FROM stage_nav.[Transfer Receipt Header] AS TRH
  ) DS
LEFT JOIN help.Enumerations AS e_dt
  ON e_dt.TableName = 'Item Ledger Entry'
    AND e_dt.ColumnName = 'Document Type'
    AND e_dt.OptionNo = DS.[DocumentType]
    AND e_dt.DataConnectionId = DS.DataConnectionID
