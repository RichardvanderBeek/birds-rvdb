SELECT
  -- system information
  SL.stage_id AS StageID
  , SL.Company_Id AS CompanyID
  , SL.data_connection_id AS DataConnectionID
  -- dimensions                                                                       
  , COALESCE(SL.[Sell-to Customer No_], SH.[Sell-to Customer No_]) AS SellToCustomerCode
  , COALESCE(SL.[Bill-to Customer No_], SH.[Bill-to Customer No_]) AS BillToCustomerCode
  , SH.[Ship-to Code] AS ShipToCustomerCode
  , SL.[Document No_] AS SalesOrderDocumentNo
  , SL.[Line No_] AS SalesOrderLineNo
  , SL.[Document Type] AS SalesOrderDocumentType
  , SL.[No_] AS ItemCode
  , SL.[Variant Code] AS ItemVariantCode
  , COALESCE(SL.[Location Code], SH.[Location Code]) AS LocationCode
  , SL.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , COALESCE(SL.[Gen_ Bus_ Posting Group], SH.[Gen_ Bus_ Posting Group]) AS GeneralBusinessPostingGroupCode
  , SL.[Unit of Measure] AS UnitOfMeasureCode
  , SL.[Shipping Agent Code] AS ShippingAgentCode
  , SL.[Shipping Agent Service Code] AS ShippingAgentServicesCode
  , SH.[Salesperson Code] AS SalesPersonPurchaserCode
  , SH.[Shipment Method Code] AS ShipmentMethodCode
  , COALESCE(SL.[Transport Method], SH.[Transport Method]) AS TransportMethodCode
  , SH.[Order Date] AS OrderDate
  , SL.[Shipment Date] AS ShipmentDate
  , SL.[Planned Delivery Date] AS PlannedDeliveryDate
  , SL.[Promised Delivery Date] AS PromisedDeliveryDate
  , SL.[Planned Shipment Date] AS PlannedShipmentDate
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures                                                                         
  , SL.[Gross Weight] AS SalesOrderShippedGrossWeight
  , SL.[Net Weight] AS SalesOrderShippedNetWeight
  , SL.[Quantity] AS SalesOrderQuantity
  , SL.[Outstanding Qty_ (Base)] AS SalesOrderOutstandingQuantity
  , SL.[Qty_ Shipped (Base)] AS SalesOrderShippedQuantity
  , SL.[Qty_ Invoiced (Base)] AS SalesOrderInvoicedQuantity
  , SL.[Qty_ to Invoice (Base)] AS SalesOrderToInvoiceQuantity
  , SL.[Qty_ to Ship (Base)] AS SalesOrderToShipQuantity
  , SL.[Qty_ Shipped Not Invd_ (Base)] AS SalesOrderShippedNotInvoicedQuantity
  --LCY measures
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      THEN (SL.[Line Amount] - SL.[Inv_ Discount Amount]) / SH.[Currency Factor]
    ELSE (SL.[Line Amount] - SL.[Inv_ Discount Amount])
    END AS SalesOrderAmount_LCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      THEN (SL.[Unit Cost (LCY)] * SL.[Quantity (Base)]) / SH.[Currency Factor]
    ELSE (SL.[Unit Cost (LCY)] * SL.[Quantity (Base)])
    END AS SalesOrderCostAmount_LCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      THEN SL.[Line Amount] / SH.[Currency Factor]
    ELSE SL.[Line Amount]
    END AS SalesOrderGrossAmount_LCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Outstanding Qty_ (Base)]) / SH.[Currency Factor]
    WHEN SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Outstanding Qty_ (Base)])
    END AS SalesOrderOutstandingAmount_LCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Shipped (Base)]) / SH.[Currency Factor]
    WHEN SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Shipped (Base)])
    END AS SalesOrderShippedAmount_LCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Invoiced (Base)]) / SH.[Currency Factor]
    WHEN SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Invoiced (Base)])
    END AS SalesOrderInvoicedAmount_LCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ to Invoice (Base)]) / SH.[Currency Factor]
    WHEN SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ to Invoice (Base)])
    END AS SalesOrderToInvoiceAmount_LCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ to Ship (Base)]) / SH.[Currency Factor]
    WHEN SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ to Ship (Base)])
    END AS SalesOrderToShipAmount_LCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Shipped Not Invd_ (Base)]) / SH.[Currency Factor]
    WHEN SL.[Quantity (Base)] <> 0
      THEN ((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Shipped Not Invd_ (Base)])
    END AS SalesOrderShippedNotInvoicedAmount_LCY
  , SL.[Inv_ Discount Amount] AS SalesOrderDiscountAmount_LCY
  --RCY measures
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      THEN ((SL.[Line Amount] - SL.[Inv_ Discount Amount]) / SH.[Currency Factor]) * ER.CrossRate
    ELSE (SL.[Line Amount] - SL.[Inv_ Discount Amount])
    END AS SalesOrderAmount_RCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      THEN ((SL.[Unit Cost (LCY)] * SL.[Quantity (Base)]) / SH.[Currency Factor]) * ER.CrossRate
    ELSE (SL.[Unit Cost (LCY)] * SL.[Quantity (Base)])
    END AS SalesOrderCostAmount_RCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      THEN (SL.[Line Amount] / SH.[Currency Factor]) * ER.CrossRate
    ELSE SL.[Line Amount]
    END AS SalesOrderGrossAmount_RCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Outstanding Qty_ (Base)]) / SH.[Currency Factor]) * ER.CrossRate
    WHEN SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Outstanding Qty_ (Base)])) * ER.CrossRate
    END AS SalesOrderOutstandingAmount_RCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Shipped (Base)]) / SH.[Currency Factor]) * ER.CrossRate
    WHEN SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Shipped (Base)])) * ER.CrossRate
    END AS SalesOrderShippedAmount_RCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Invoiced (Base)]) / SH.[Currency Factor]) * ER.CrossRate
    WHEN SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Invoiced (Base)])) * ER.CrossRate
    END AS SalesOrderInvoicedAmount_RCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ to Invoice (Base)]) / SH.[Currency Factor]) * ER.CrossRate
    WHEN SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ to Invoice (Base)])) * ER.CrossRate
    END AS SalesOrderToInvoiceAmount_RCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ to Ship (Base)]) / SH.[Currency Factor]) * ER.CrossRate
    WHEN SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ to Ship (Base)])) * ER.CrossRate
    END AS SalesOrderToShipAmount_RCY
  , CASE 
    WHEN SH.[Currency Factor] <> 0
      AND SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Shipped Not Invd_ (Base)]) / SH.[Currency Factor]) * ER.CrossRate
    WHEN SL.[Quantity (Base)] <> 0
      THEN (((([Line Amount] - [Inv_ Discount Amount]) / [Quantity (Base)]) * SL.[Qty_ Shipped Not Invd_ (Base)])) * ER.CrossRate
    END AS SalesOrderShippedNotInvoicedAmount_RCY
  , SL.[Inv_ Discount Amount] * ER.CrossRate AS SalesOrderDiscountAmount_RCY
FROM stage_nav.[Sales Line] AS SL
LEFT JOIN stage_nav.[Sales Header] AS SH
  ON SL.[Document Type] = SH.[Document Type]
    AND SL.[Document No_] = SH.[No_]
    AND SL.Company_Id = SH.Company_ID
    AND SL.Data_connection_id = SH.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON SL.[Shipment Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = SL.company_id
    AND ER.DataConnectionID = SL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON SL.[Dimension Set ID] = FD.dimensionsetno
    AND SL.company_id = FD.companyid
    AND SL.data_connection_id = FD.DataConnectionID
WHERE SL.[Document Type] IN (
    0
    , 1
    , 4
    )
