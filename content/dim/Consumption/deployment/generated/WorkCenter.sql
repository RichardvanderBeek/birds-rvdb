SELECT 5 AS EntryType -- Consumption
  , 'Consumption' AS EntryTypeDesc
  , a.Type AS SourceType -- Work Center = 0, Machine Center = 1, Item = 3
  , 'Work Center' AS SourceTypeDesc
  , a.No_ AS ConsumptionCode
  , a.No_ + ISNULL(' - ' + NULLIF(b.NAME, ''), '') AS ConsumptionCodeDesc
  , a.[Order No_] AS ProductionOrderCode
  , COALESCE(NULLIF(b.[Work Center Group Code], ''), 'N/A') AS ConsumptionGroup1Code -- Work Center Group Code
  , COALESCE(NULLIF(b.[Work Center Group Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(c.NAME, ''), 'N/A') AS ConsumptionGroup1CodeDesc
  , COALESCE(NULLIF(b.[Work Center Group Code], ''), 'N/A') AS ConsumptionGroup2Code -- WorkCenter Group Code
  , COALESCE(NULLIF(b.[Work Center Group Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(c.NAME, ''), 'N/A') AS ConsumptionGroup2CodeDesc
  , 'N/A' AS ProductGroupCode
  , 'N/A' AS ProductGroupCodeDesc
  , a.company_id AS CompanyID
  , a.data_connection_id AS DataConnectionID
  , 1 AS ConsumptionKey
, 'WorkCenter'AS ConsumptionKeyDesc
FROM stage_nav.[Capacity Ledger Entry] a
LEFT JOIN stage_nav.[Work Center] b
  ON a.No_ = b.No_
    AND a.company_id = b.company_id
    AND a.data_connection_id = b.data_connection_id
LEFT JOIN stage_nav.[Work Center Group] c
  ON b.[Work Center Group Code] = c.Code
    AND b.company_id = c.company_id
    AND b.data_connection_id = c.data_connection_id
WHERE a.Type = 0 -- Work Center
  AND a.No_ <> ''
