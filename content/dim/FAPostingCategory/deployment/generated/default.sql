SELECT
  --   Business Key                                                                                       
  E_T.OptionNo AS PostingCategoryCode
  --  Attributes
  , COALESCE(NULLIF(E_T.OptionValue, ''), 'N/A') AS PostingCategoryDescription
  , E_T.DataConnectionId AS DataConnectionID
FROM help.Enumerations AS E_T
WHERE E_T.TableName = 'FA Ledger Entry'
  AND E_T.ColumnName = 'FA Posting Category'
