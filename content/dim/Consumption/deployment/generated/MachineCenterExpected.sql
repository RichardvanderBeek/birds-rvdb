SELECT 5 AS EntryType -- Consumption
  , 'Consumption' AS EntryTypeDesc
  , a.Type AS SourceType -- Work Center = 0, Machine Center = 1, Item = 3
  , 'Machine Center' AS SourceTypeDesc
  , a.No_ AS ConsumptionCode
  , a.No_ + ISNULL(' - ' + NULLIF(c.NAME, ''), '') AS ConsumptionCodeDesc
  , a.[Prod_ Order No_] AS ProductionOrderCode
  , COALESCE(NULLIF(d.[Work Center Group Code], ''), 'N/A') AS ConsumptionGroup1Code
  , COALESCE(NULLIF(d.[Work Center Group Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(e.NAME, ''), 'N/A') AS ConsumptionGroup1CodeDesc
  , COALESCE(NULLIF(d.[Work Center Group Code], ''), 'N/A') AS ConsumptionGroup2Code
  , COALESCE(NULLIF(d.[Work Center Group Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(e.NAME, ''), 'N/A') AS ConsumptionGroup2CodeDesc
  , 'N/A' AS ProductGroupCode
  , 'N/A' AS ProductGroupCodeDesc
  , a.company_id AS CompanyID
  , a.data_connection_id AS DataConnectionID
  , 6 AS ConsumptionKey
  , 'MachineCenterExpected' AS ConsumptionKeyDesc
FROM stage_nav.[Prod_ Order Routing Line] a
LEFT JOIN stage_nav.[Machine Center] c
  ON a.No_ = c.No_
    AND a.company_id = c.company_id
    AND a.data_connection_id = c.data_connection_id
LEFT JOIN stage_nav.[Work Center] d
  ON c.[Work Center No_] = d.No_
    AND c.company_id = d.company_id
    AND c.data_connection_id = d.data_connection_id
LEFT JOIN stage_nav.[Work Center Group] e
  ON d.[Work Center Group Code] = e.Code
    AND d.company_id = e.company_id
    AND d.data_connection_id = e.data_connection_id
WHERE a.Type = 1 -- Machine Center
  AND a.No_ <> ''
