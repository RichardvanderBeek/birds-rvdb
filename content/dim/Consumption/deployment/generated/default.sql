SELECT a.[Entry Type] AS EntryType
  , 'Consumption' AS EntryTypeDesc
  , a.[Source Type] AS SourceType -- Work Center = 0, Machine Center = 1, Item = 3
  , 'Item' AS SourceTypeDesc
  , a.[Item No_] AS ConsumptionCode
  , a.[Item No_] + ISNULL(' - ' + NULLIF(b.Description, ''), '') AS ConsumptionCodeDesc
  , a.[Order No_] AS ProductionOrderCode
  , COALESCE(NULLIF(b.[Inventory Posting Group], ''), 'N/A') AS ConsumptionGroup1Code
  , COALESCE(NULLIF(b.[Inventory Posting Group], ''), 'N/A') + ' - ' + COALESCE(NULLIF(c.Description, ''), 'N/A') AS ConsumptionGroup1CodeDesc
  , COALESCE(NULLIF(b.[Item Category Code], ''), 'N/A') AS ConsumptionGroup2Code
  , COALESCE(NULLIF(b.[Item Category Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(d.Description, ''), 'N/A') AS ConsumptionGroup2CodeDesc
  , COALESCE(NULLIF(b.[Product Group Code], ''), 'N/A') AS ProductGroupCode
  , COALESCE(NULLIF(b.[Product Group Code], ''), 'N/A') + COALESCE(' - ' + NULLIF(e.Description, ''), '') AS ProductGroupCodeDesc
  , a.company_id AS CompanyID
  , a.data_connection_id AS DataConnectionID
  , 0 AS ConsumptionKey
  , 'Consumption' AS ConsumptionKeyDesc
FROM stage_nav.[Item Ledger Entry] a
LEFT JOIN stage_nav.Item b
  ON a.[Item No_] = b.No_
    AND a.company_id = b.company_id
    AND a.data_connection_id = b.data_connection_id
LEFT JOIN stage_nav.[Inventory Posting Group] c
  ON b.[Inventory Posting Group] = c.Code
    AND b.company_id = c.company_id
    AND b.data_connection_id = c.data_connection_id
LEFT JOIN stage_nav.[Item Category] d
  ON b.[Item Category Code] = d.Code
    AND b.company_id = d.company_id
    AND b.data_connection_id = d.data_connection_id
LEFT JOIN stage_nav.[Product Group] e
  ON b.[Product Group Code] = e.Code
    AND b.[Item Category Code] = e.[Item Category Code]
    AND b.company_id = e.company_id
    AND b.data_connection_id = e.data_connection_id
WHERE a.[Entry Type] = 5 -- Consumption
  AND a.[Item No_] <> ''
