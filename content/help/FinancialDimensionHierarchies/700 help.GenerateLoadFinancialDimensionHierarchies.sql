/****************************************************************************************************
  Functionality: Generates a procedure that loads the FinancialDimensionhierarchies, 
                this is included in the processflow, to regerenate the procedure for every process
  Created by:    JvL	Date:	2019/02/26
  Date 		Changed by 	Ticket/Change 	Description
*****************************************************************************************************/
EXEC dbo.drop_object @object = N'help.GenerateLoadFinancialDimensionHierarchies', @type = N'P' ;
GO
CREATE PROCEDURE help.GenerateLoadFinancialDimensionHierarchies
  @component_execution_id INT      = -1
, @execution_flag         NCHAR(1) = 'N'
, @load_type              TINYINT  = 0
, @debug                  BIT      = 0
, @inserted               INT      = NULL OUTPUT
, @updated                INT      = NULL OUTPUT
, @deleted                INT      = NULL OUTPUT
AS
BEGIN

  DECLARE @select_columns     NVARCHAR(MAX) = N''
        , @insert_columns     NVARCHAR(MAX) = N''
        , @outer_apply        NVARCHAR(MAX) = N''
        , @coalesce_leaf_code NVARCHAR(MAX) = CHAR(10) + N'  , COALESCE('
        , @coalesce_leaf_name NVARCHAR(MAX) = CHAR(10) + N'  , COALESCE(' ;
  DECLARE @table_object_identifier NVARCHAR(255) = N'help.FinancialDimensionHierarchies' ;
  DECLARE @procedure_object_identifier NVARCHAR(255) = N'help.LoadFinancialDimensionHierarchies' ;
  DECLARE @create_procedure NVARCHAR(MAX) ;


  /****************************************************************************************************
      Functionality:  Determine to generate columns and outerapply's for the different parts of the query
  *****************************************************************************************************/

  WITH hierarchy_levels (lvl) AS (
    SELECT help.GetFinancialDimensionHierachiesMaxLevel() -- Seed Row
    UNION ALL
    SELECT hierarchy_levels.lvl - 1 -- Recursion
      FROM hierarchy_levels
     WHERE hierarchy_levels.lvl > 0
  )
  SELECT @select_columns +=+CHAR(10) + N'  ' + N', [l' + CAST(cte.lvl AS NVARCHAR(10)) + N'].[level_code] AS LevelCode' + CAST(cte.lvl AS NVARCHAR(10))
                           + CHAR(10) + N'  ' + N', [l' + CAST(cte.lvl AS NVARCHAR(10)) + N'].[level_name] AS LevelName' + CAST(cte.lvl AS NVARCHAR(10))
       , @insert_columns +=+CHAR(10) + N'  ' + N', LevelCode' + CAST(cte.lvl AS NVARCHAR(10)) + CHAR(10) + N'  ' + N', LevelName'
                           + CAST(cte.lvl AS NVARCHAR(10))
       , @outer_apply += CASE
                           WHEN cte.lvl > 0 THEN
                             CHAR(10) + ' OUTER APPLY
       OPENJSON(l'                               + CAST((cte.lvl - 1) AS NVARCHAR(10))
                             + '.children)
         WITH ( level_code NVARCHAR(40) ''$.code''
             , level_name NVARCHAR(40) ''$.name''
             , children NVARCHAR(MAX) ''$.children'' AS JSON) AS l' + CAST(cte.lvl AS NVARCHAR(10))
                           ELSE '' --outer apply is not required for the level 0
                         END
    FROM hierarchy_levels AS cte
   ORDER BY cte.lvl ASC ;

  -- to determine the leaf value a coalesce is created on lvln to lvl0
  WITH hierarchy_levels (lvl) AS (
    SELECT help.GetFinancialDimensionHierachiesMaxLevel() -- Seed Row
    UNION ALL
    SELECT hierarchy_levels.lvl - 1 -- Recursion
      FROM hierarchy_levels
     WHERE hierarchy_levels.lvl > 0
  )
  SELECT @coalesce_leaf_code += N'[l' + CAST(cte.lvl AS NVARCHAR(10)) + N'].[level_code], '
       , @coalesce_leaf_name += N'[l' + CAST(cte.lvl AS NVARCHAR(10)) + N'].[level_name], '
    FROM hierarchy_levels AS cte
   ORDER BY cte.lvl DESC ;

  SELECT @coalesce_leaf_code = LEFT(@coalesce_leaf_code, LEN(@coalesce_leaf_code) - 1) + N') AS LeafCode' ;
  SELECT @coalesce_leaf_name = LEFT(@coalesce_leaf_name, LEN(@coalesce_leaf_name) - 1) + N') AS LeafName' ;

  /****************************************************************************************************
      Functionality:  Create the Procedure SQL and Deploy the Procedure  
  *****************************************************************************************************/

  SELECT @create_procedure
    = N'CREATE PROCEDURE ' + @procedure_object_identifier + CHAR(10)
      + N'@component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @debug                  BIT     = 0
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS'                          + CHAR(10)
      + N'
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ; 

  DECLARE @json NVARCHAR(MAX) ;
  SELECT @json = help.FinancialDimensionHierarchiesAsJson() ;
  IF @debug = 1 
  BEGIN
    EXEC dbo.long_print @json
  END  ;  ' ;


  SELECT @create_procedure += CHAR(10) + N'
  INSERT INTO '               + @table_object_identifier
                              + N' (
    StageId
  , ComponentExecutionId
  , CompanyId
  , DataConnectionId
  , DimensionCode '           + @insert_columns + CHAR(10) + N', Code' + CHAR(10) + N', Name)' ;
  SELECT @create_procedure +=+CHAR(10)
                             + N'SELECT 
    l0.stage_id
  , @component_execution_id
  , l0.company_id 
  , l0.data_connection_id
  , l0.dimension_code '      + @select_columns + @coalesce_leaf_code + @coalesce_leaf_name ;
  SELECT @create_procedure += CHAR(10)
                              + N'FROM
       OPENJSON(@json)
        OUTER APPLY
       OPENJSON([value])
         WITH (stage_id INT ''$.stage_id''
             , data_connection_id INT ''$.data_connection_id''
             , company_id INT ''$.company_id''
             , dimension_code NVARCHAR(40) ''$.dimension_code''
             , level_code NVARCHAR(40) ''$.code''
             , level_name NVARCHAR(40) ''$.name''
             , children NVARCHAR(MAX) ''$.children'' AS JSON) AS l0' + @outer_apply ;

  SELECT @create_procedure += CHAR(10) + N' SELECT @inserted = @@ROWCOUNT ;' ;

  SELECT @create_procedure +=+CHAR(10) + N'END ;' ;


  EXEC dbo.drop_object @object = @procedure_object_identifier, @type = N'P', @debug = @debug ;
  IF @debug = 1 BEGIN
PRINT 'GO' ;
EXEC dbo.long_print @str = @create_procedure ;
PRINT 'GO' ;
  END ;
  ELSE BEGIN

EXEC sys.sp_executesql @create_procedure ;
  END ;

END ;




GO

EXEC help.GenerateLoadFinancialDimensionHierarchies @debug = 0 ;