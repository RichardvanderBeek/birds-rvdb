WITH cte_productionsum
AS (
  SELECT VE.stage_id
    , VE.execution_flag
    , VE.execution_timestamp
    , VE.company_id
    , VE.data_connection_id
    , VE.[Item Ledger Entry No_]
    , VE.[Entry Type]
    , SUM(VE.[Item Ledger Entry Quantity]) AS ItemLedgerEntryQuantity
    , SUM(VE.[Invoiced Quantity]) AS InvoicedQuantity
    , SUM(VE.[Cost Amount (Actual)]) AS CostAmountActual
  FROM stage_nav.[Value Entry] AS VE
  WHERE VE.[Item Ledger Entry Type] IN (
      5
      , 6
      ) -- Consumption, Output
  GROUP BY VE.stage_id
    , VE.execution_flag
    , VE.execution_timestamp
    , VE.company_id
    , VE.data_connection_id
    , VE.[Item Ledger Entry No_]
    , VE.[Entry Type]
  )
SELECT
  -- system information
  ILE.stage_id AS StageID
  , ILE.Company_Id AS CompanyID
  , ILE.data_connection_id AS DataConnectionID
  , 1 AS ManufacturingType
  -- dimensions
  , ILE.[Entry No_] AS EntryNo
  , ILE.[Entry Type] AS EntryType
  , ILE.[Posting Date] AS PostingDate
  , ILE.[Order No_] AS ProductionOrderCode
  , ILE.[Item No_] AS ItemCode
  , CASE 
    WHEN ILE.[Entry Type] = 6
      AND ILE.[Item No_] <> ILE.[Source No_]
      THEN ILE.[Item No_]
    ELSE ILE.[Source No_]
    END AS OutputItemCode
  , CAST(NULL AS NVARCHAR) AS WorkCenterCode
  , ILE.[Source Type] AS SourceType
  , CAST(NULL AS NVARCHAR) AS ScrapCode
  , CAST(NULL AS NVARCHAR) AS StopCode
  , ILE.[Location Code] AS LocationCode
  , CAST(NULL AS TINYINT) AS SubcontractingCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , CASE 
    WHEN ILE.[Entry Type] = 6
      THEN CTE.ItemLedgerEntryQuantity
    ELSE 0
    END AS ActualOutputQuantity
  , CASE 
    WHEN ILE.[Entry Type] = 5
      THEN (- 1) * CTE.InvoicedQuantity
    ELSE 0
    END AS ActualConsumptionQuantity
  , CASE 
    WHEN ILE.[Entry Type] = 5
      THEN (- 1) * CTE.InvoicedQuantity
    ELSE CTE.InvoicedQuantity
    END AS ActualTotalQuantity
  , CASE 
    WHEN ILE.[Entry Type] = 6
      THEN CTE.CostAmountActual
    ELSE 0
    END AS ActualOutputCost
  , CASE 
    WHEN ILE.[Entry Type] = 5
      THEN (- 1) * CTE.CostAmountActual
    ELSE 0
    END AS ActualConsumptionCost
  , CASE 
    WHEN ILE.[Entry Type] = 5
      THEN (- 1) * CTE.CostAmountActual
    ELSE CTE.CostAmountActual
    END AS ActualTotalCost
  , CASE 
    WHEN ILE.[Entry Type] = 5
      AND CTE.[Entry Type] = 3
      THEN (- 1) * CTE.CostAmountActual
    ELSE 0
    END AS ActualOverheadCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ScrapQuantity
  , CAST(NULL AS DECIMAL(38, 20)) AS SetupTime
  , CAST(NULL AS DECIMAL(38, 20)) AS RunTime
  , CAST(NULL AS DECIMAL(38, 20)) AS StopTime
FROM stage_nav.[Item Ledger Entry] AS ILE
LEFT JOIN cte_productionsum AS CTE
  ON ILE.[Entry No_] = CTE.[Item Ledger Entry No_]
    AND ILE.company_id = CTE.company_id
    AND ILE.data_connection_id = CTE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON ILE.[Dimension Set ID] = FD.DimensionSetNo
    AND ILE.company_id = FD.CompanyID
    AND ILE.data_connection_id = FD.DataConnectionID
WHERE ILE.[Entry Type] IN (
    5
    , 6
    ) -- Consumption, Output
