SELECT
  -- System Information
  D.stage_id AS StageID
  , D.company_id AS CompanyID
  , D.data_connection_id AS DataConnectionID
  -- Business Key                                                                                       
  , D.Code AS DepreciationBookCode
  -- Attributes
  , ISNULL(NULLIF(D.Description, ''), 'N/A') AS DepreciationBookDescription
  , D.Code + ISNULL(' - ' + NULLIF(D.Description, ''), '') AS DepreciationBookCodeDescription
FROM stage_nav.[Depreciation Book] AS D
