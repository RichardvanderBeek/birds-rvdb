SELECT 5 AS EntryType -- Consumption
  , 'Consumption' AS EntryTypeDesc
  , a.Type AS SourceType
  , 'Machine Center' AS SourceTypeDesc
  , a.No_ AS ConsumptionCode
  , a.No_ + ISNULL(' - ' + NULLIF(b.NAME, ''), '') AS ConsumptionCodeDesc
  , a.[Order No_] AS ProductionOrderCode
  , COALESCE(NULLIF(c.[Work Center Group Code], ''), 'N/A') AS ConsumptionGroup1Code
  , COALESCE(NULLIF(c.[Work Center Group Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(d.NAME, ''), 'N/A') AS ConsumptionGroup1CodeDesc
  , COALESCE(NULLIF(c.[Work Center Group Code], ''), 'N/A') AS ConsumptionGroup2Code
  , COALESCE(NULLIF(c.[Work Center Group Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(d.NAME, ''), 'N/A') AS ConsumptionGroup2CodeDesc
  , 'N/A' AS ProductGroupCode
  , 'N/A' AS ProductGroupCodeDesc
  , a.company_id AS CompanyID
  , a.data_connection_id AS DataConnectionID
  , 2 AS ConsumptionKey
, 'MachineCenter'AS ConsumptionKeyDesc
FROM stage_nav.[Capacity Ledger Entry] a
LEFT JOIN stage_nav.[Machine Center] b
  ON a.No_ = b.No_
    AND a.company_id = b.company_id
    AND a.data_connection_id = b.data_connection_id
LEFT JOIN stage_nav.[Work Center] c
  ON b.[Work Center No_] = c.No_
    AND b.company_id = c.company_id
    AND b.data_connection_id = c.data_connection_id
LEFT JOIN stage_nav.[Work Center Group] d
  ON c.[Work Center Group Code] = d.Code
    AND c.company_id = d.company_id
    AND c.data_connection_id = d.data_connection_id
WHERE a.Type = 1 -- Machine Center
  AND a.No_ <> ''
