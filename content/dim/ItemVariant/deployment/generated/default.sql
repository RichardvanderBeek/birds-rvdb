SELECT
  -- system information
  IV.stage_id AS StageID
  , IV.company_id AS CompanyID
  , IV.data_connection_id AS DataConnectionID
  -- business key
  , IV.[Item No_] AS ItemCode
  , IV.[Code] AS ItemVariantCode
  --  attributes                        
  , COALESCE(NULLIF(IV.Description, ''), 'N/A') AS ItemVariantDescription
  , COALESCE(NULLIF(IV.[Item No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(IV.Description, ''), 'N/A') AS ItemVariantCodeDescription
  , COALESCE(NULLIF(IV.[Description 2], ''), 'N/A') AS ItemVariantDescription2
  , COALESCE(NULLIF(IV.[Item No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(IV.[Description 2], ''), 'N/A') AS ItemVariantCodeDescription2
FROM stage_nav.[Item Variant] AS IV
