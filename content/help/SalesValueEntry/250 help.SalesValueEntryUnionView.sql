/***********************************************************************************
Functionality:  This SQL script creates the help.SalesValueEntryUnionView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to SalesLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesValueEntryUnionView', @type = N'V' ;
GO
CREATE VIEW help.SalesValueEntryUnionView
AS
  SELECT SSLV.StageID
       , SSLV.CompanyID
       , SSLV.DataConnectionID
       , SSLV.execution_timestamp
       , SSLV.EntryNo
       , SSLV.DocumentType
       , SSLV.DocumentNo
       , SSLV.PostingDate
       , SSLV.DocumentPostingDate
       , SSLV.SelltoCustomerNo
       , SSLV.BilltoCustomerNo
       , SSLV.CurrencyFactor
       , SSLV.CurrencyCode
       , SSLV.Type
    FROM help.SalesShipmentLineView AS SSLV
  UNION ALL
  SELECT SILV.StageID
       , SILV.CompanyID
       , SILV.DataConnectionID
       , SILV.execution_timestamp
       , SILV.EntryNo
       , SILV.DocumentType
       , SILV.DocumentNo
       , SILV.PostingDate
       , SILV.DocumentPostingDate
       , SILV.SelltoCustomerNo
       , SILV.BilltoCustomerNo
       , SILV.CurrencyFactor
       , SILV.CurrencyCode
       , SILV.Type
    FROM help.SalesInvoiceLineView AS SILV
  UNION ALL
  SELECT RRLV.StageID
       , RRLV.CompanyID
       , RRLV.DataConnectionID
       , RRLV.execution_timestamp
       , RRLV.EntryNo
       , RRLV.DocumentType
       , RRLV.DocumentNo
       , RRLV.PostingDate
       , RRLV.DocumentPostingDate
       , RRLV.SelltoCustomerNo
       , RRLV.BilltoCustomerNo
       , RRLV.CurrencyFactor
       , RRLV.CurrencyCode
       , RRLV.Type
    FROM help.ReturnReceiptLineView AS RRLV
  UNION ALL
  SELECT SCMLV.StageID
       , SCMLV.CompanyID
       , SCMLV.DataConnectionID
       , SCMLV.execution_timestamp
       , SCMLV.EntryNo
       , SCMLV.DocumentType
       , SCMLV.DocumentNo
       , SCMLV.PostingDate
       , SCMLV.DocumentPostingDate
       , SCMLV.SelltoCustomerNo
       , SCMLV.BilltoCustomerNo
       , SCMLV.CurrencyFactor
       , SCMLV.CurrencyCode
       , SCMLV.Type
    FROM help.SalesCreditMemoLineView AS SCMLV
  UNION ALL
  SELECT SVEV.StageID
       , SVEV.CompanyID
       , SVEV.DataConnectionID
       , SVEV.execution_timestamp
       , SVEV.EntryNo
       , SVEV.DocumentType
       , SVEV.DocumentNo
       , SVEV.PostingDate
       , SVEV.DocumentPostingDate
       , SVEV.SelltoCustomerNo
       , SVEV.BilltoCustomerNo
       , SVEV.CurrencyFactor
       , SVEV.CurrencyCode
       , SVEV.Type
    FROM help.SalesValueEntryView AS SVEV ;
