WITH LastApplicationDate_CTE
AS (
  SELECT company_id AS CompanyID
    , data_connection_id AS DataConnectionID
    , [Cust_ Ledger Entry No_] AS CustLedgerEntryNo
    , MAX([Posting Date]) AS LastApplicationDate
  FROM stage_nav.[Detailed Cust_ Ledg_ Entry]
  WHERE [Entry Type] = 2 --Application
  GROUP BY company_id
    , data_connection_id
    , [Cust_ Ledger Entry No_]
  )
SELECT
  -- system information
  DCLE.stage_id AS StageID
  , DCLE.company_id AS CompanyID
  , DCLE.data_connection_id AS DataConnectionID
  -- dimensions
  , CLE.[Customer No_] AS BillToCustomerCode
  , CLE.[Sell-to Customer No_] AS SellToCustomerCode
  , CLE.[Customer Posting Group] AS CustomerPostingGroupCode
  , DCLE.[Cust_ Ledger Entry No_] AS CustomerInvoiceCode
  , CLE.[Salesperson Code] AS SalespersonPurchaserCode
  , DCLE.[Posting Date] AS PostingDate
  , COALESCE(NULLIF(CLE.[Due Date], '1753-01-01 00:00:00'), CLE.[Posting Date]) AS DueDate
  , CASE 
    WHEN CLE.[Open] = 1
      THEN NULL
    ELSE COALESCE(NULLIF(CLE.[Closed at Date], '1753-01-01 00:00:00'), NULLIF(LAD.[LastApplicationDate], '1753-01-01 00:00:00'))
    END AS PaidDate
  , DCLE.[Currency Code] AS CurrencyCode
  , CASE 
    WHEN DCLE.[Entry Type] = 1
      AND CLE.[Document Type] IN (
        2
        , 3
        )
      THEN 1
    ELSE 0
    END AS IsInvoiceCode
  , CASE 
    WHEN DCLE.[Credit Amount (LCY)] <> 0
      THEN 1
    ELSE 0
    END AS IsCreditCode
  , COALESCE(CASE 
      WHEN DCLE.[Entry Type] = 1
        AND CLE.[Document Type] IN (
          2
          , 3
          )
        THEN 1
      ELSE 0
      END, - 1) AS IsInvoice
  , COALESCE(CASE 
      WHEN DCLE.[Credit Amount (LCY)] <> 0
        THEN 1
      ELSE 0
      END, - 1) AS IsCredit
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , DCLE.[Amount (LCY)] AS Amount_LCY
  , DCLE.[Debit Amount (LCY)] AS AmountDebit_LCY
  , DCLE.[Credit Amount (LCY)] AS AmountCredit_LCY
  , CASE 
    WHEN DCLE.[Entry Type] = 1
      THEN CLE.[Inv_ Discount (LCY)]
    ELSE 0
    END AS AmountDiscount_LCY
  -- measures_RCY
  , DCLE.[Amount (LCY)] * ER.CrossRate AS Amount_RCY
  , DCLE.[Debit Amount (LCY)] * ER.CrossRate AS AmountDebit_RCY
  , DCLE.[Credit Amount (LCY)] * ER.CrossRate AS AmountCredit_RCY
  , CASE 
    WHEN DCLE.[Entry Type] = 1
      THEN CLE.[Inv_ Discount (LCY)]
    ELSE 0
    END * ER.CrossRate AS AmountDiscount_RCY
  -- measures_PCY
  , DCLE.Amount AS Amount_PCY
FROM stage_nav.[Detailed Cust_ Ledg_ Entry] AS DCLE
LEFT JOIN stage_nav.[Cust_ Ledger Entry] AS CLE
  ON CLE.[Entry No_] = DCLE.[Cust_ Ledger Entry No_]
    AND CLE.company_id = DCLE.company_id
    AND CLE.data_connection_id = DCLE.data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON DCLE.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = DCLE.company_id
    AND ER.DataConnectionID = DCLE.data_connection_id
LEFT JOIN LastApplicationDate_CTE AS LAD
  ON CLE.[Entry No_] = LAD.CustLedgerEntryNo
    AND CLE.company_id = LAD.CompanyID
    AND CLE.data_connection_id = LAD.DataConnectionID
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON CLE.[Dimension Set ID] = FD.DimensionSetNo
    AND CLE.company_id = FD.CompanyID
    AND CLE.data_connection_id = FD.DataConnectionID
