/***********************************************************************************
Functionality:  This SQL script creates the help.PurchaseCreditMemoLineItemLedgerEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.PurchaseCreditMemoLineItemLedgerEntryView', @type = N'V' ;
GO
CREATE VIEW help.PurchaseCreditMemoLineItemLedgerEntryView
AS
  SELECT
    -- system information
                    PCML.stage_id                                      AS StageID
                  , PCML.company_id                                    AS CompanyID
                  , PCML.data_connection_id                            AS DataConnectionID
                  -- business key
                  , ILE.[Entry No_]                                    AS EntryNo
                  , ILE.[Document Type]                                AS DocumentType
                  , ILE.[Document No_]                                 AS DocumentNo
                  , ILE.[Posting Date]                                 AS PostingDate
                  , COALESCE(PCMH.[Posting Date], PCML.[Posting Date]) AS DocumentPostingDate
                  , PCML.[Buy-from Vendor No_]                         AS BuyFromVendorNo
                  , PCML.[Pay-to Vendor No_]                           AS PayToVendorNo
    FROM            stage_nav.[Purch_ Cr_ Memo Line] AS PCML
    LEFT OUTER JOIN stage_nav.[Purch_ Cr_ Memo Hdr_] AS PCMH ON PCML.[Document No_]     = PCMH.[No_]
                                                            AND PCML.company_id         = PCMH.company_id
                                                            AND PCML.data_connection_id = PCMH.data_connection_id
   INNER JOIN       stage_nav.[Item Ledger Entry]    AS ILE ON ILE.[Document Type]      = 8 --Credit Memo
                                                           AND PCML.[Document No_]      = ILE.[Document No_]
                                                           AND PCML.[Line No_]          = ILE.[Document Line No_]
                                                           AND ILE.[Entry Type]         = 0 --Purchase  
                                                           AND PCML.company_id          = ILE.company_id
                                                           AND PCML.data_connection_id  = ILE.data_connection_id
   WHERE            PCML.execution_flag <> 'D'
     AND            PCMH.execution_flag            <> 'D'
     AND            ILE.execution_flag             <> 'D' ;
GO
