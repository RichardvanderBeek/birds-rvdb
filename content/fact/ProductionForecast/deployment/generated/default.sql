SELECT
  -- system information
  PFE.stage_id AS StageID
  , PFE.Company_Id AS CompanyID
  , PFE.data_connection_id AS DataConnectionID
  -- dimensions
  , PFE.[Entry No_] AS EntryNo
  , PFE.[Forecast Date] AS ForecastDate
  , PFE.[Item No_] AS ItemCode
  , PFE.[Production Forecast Name] AS ProductionForecastCode
  , PFE.[Location Code] AS LocationCode
  -- measures
  , PFE.[Forecast Quantity] AS ForecastQuantity
  , PFE.[Forecast Quantity (Base)] AS ForecastQuantityBase
FROM stage_nav.[Production Forecast Entry] AS PFE
