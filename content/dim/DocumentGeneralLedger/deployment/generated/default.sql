SELECT
  -- system information
  GLE.company_id AS CompanyID
  , GLE.data_connection_id AS DataConnectionID
  -- business key
  , GLE.[Entry No_] AS EntryNo
  , GLE.[Document No_] AS DocumentCode
  , GLE.[Document Type] AS DocumentTypeCode
  , COALESCE(e_DT.OptionValue, 'N/A') AS DocumentTypeDescription
  , GLE.[Gen_ Posting Type] AS GeneralPostingTypeCode
  , COALESCE(e_gpt.OptionValue, 'N/A') AS GeneralPostingTypeDescription
  , COALESCE(NULLIF(GLE.[Document Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS DocumentDate
  , COALESCE(NULLIF(GLE.[Posting Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS PostingDate
  , COALESCE(NULLIF(GLE.Description, ''), 'N/A') AS DocumentDescription
FROM stage_nav.[G_L Entry] AS GLE
LEFT JOIN help.Enumerations AS e_DT
  ON e_DT.OptionNo = GLE.[Document Type]
    AND e_DT.ColumnName = 'Document Type'
    AND e_DT.TableName = 'G/L Entry'
    AND e_DT.dataconnectionid = GLE.data_connection_id
LEFT JOIN help.Enumerations AS e_gpt
  ON e_gpt.OptionNo = GLE.[Gen_ Posting Type]
    AND e_gpt.ColumnName = 'Gen. Posting Type'
    AND e_gpt.TableName = 'G/L Entry'
    AND e_gpt.dataconnectionid = GLE.data_connection_id
GROUP BY GLE.company_id
  , GLE.data_connection_id
  , GLE.[Entry No_]
  , GLE.[Document No_]
  , GLE.[Document Type]
  , e_DT.OptionValue
  , GLE.[Gen_ Posting Type]
  , e_gpt.OptionValue
  , GLE.[Document Date]
  , GLE.[Posting Date]
  , GLE.Description
