SELECT
  -- system information
  SAS.stage_id AS StageID
  , SAS.company_id AS CompanyID
  , SAS.data_connection_id AS DataConnectionID
  -- business key
  , SAS.Code AS ShippingAgentServicesCode
  , SAS.[Shipping Agent Code] AS ShippingAgentCode
  --  attributes                        
  , COALESCE(NULLIF(SAS.Description, ''), 'N/A') AS ShippingAgentServicesDescription
  , COALESCE(NULLIF(SAS.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(SAS.Description, ''), 'N/A') AS ShippingAgentServicesCodeDescription
FROM stage_nav.[Shipping Agent Services] AS SAS
