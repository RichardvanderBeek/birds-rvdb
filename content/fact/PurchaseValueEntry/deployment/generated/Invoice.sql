SELECT
  -- system information
  PIL.stage_id AS StageID
  , PIL.Company_Id AS CompanyID
  , PIL.data_connection_id AS DataConnectionID
  , 2 AS PurchaseValueEntryType
  -- business key
  , PIL.[Document No_] AS DocumentNo
  , PIL.[Type] AS PurchaseLineTypeCode
  , 0 AS ItemLedgerEntryNo
  , 0 AS ValueEntryNo
  -- Other fields
  , 0 AS SourceType
  , CAST(NULL AS NVARCHAR(20)) AS SourcePostingGroup
  -- dimensions                                                                       
  , CAST(NULL AS NVARCHAR(20)) AS ItemCode
  , CASE 
    WHEN PIL.[Type] = 1
      THEN PIL.[No_]
    ELSE ''
    END AS GeneralLedgerAccountCode
  , CASE 
    WHEN PIL.[Type] = 3
      THEN PIL.[No_]
    ELSE ''
    END AS ResourceCode
  , CASE 
    WHEN PIL.[Type] = 4
      THEN PIL.[No_]
    ELSE ''
    END AS FixedAssetCode
  , CASE 
    WHEN PIL.[Type] = 5
      THEN PIL.[No_]
    ELSE ''
    END AS ItemChargeCode
  , PIH.[Posting Date] AS DocumentPostingDate
  , PIL.[Posting Date] AS PostingDate
  , PIH.[Buy-from Vendor No_] AS BuyFromVendorCode
  , PIH.[Pay-to Vendor No_] AS PayToVendorCode
  , 6 AS DocumentTypeCode -- Purchase Invoice
  , PIL.[Location Code] AS LocationCode
  , CAST(NULL AS NVARCHAR(20)) AS ItemVariantCode
  , PIH.[Source Code] AS SourceCode
  , PIL.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , PIL.[Gen_ Bus_ Posting Group] AS GeneralBusinessPostingGroupCode
  , PIH.[Reason Code] AS ReasonCode
  , PIH.[Purchaser Code] AS SalesPersonPurchaserCode
  , PIH.[Currency Code] AS CurrencyCode
  , CAST(NULL AS NVARCHAR(20)) AS InventoryPostingGroupCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , PIL.[Quantity] AS InvoicedQuantity
  , PIL.[Unit Cost (LCY)] * PIL.Quantity AS PurchaseAmount_LCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS PurchaseAmountExpected_LCY
  , CASE 
    WHEN PIH.[Currency Factor] <> 0
      THEN PIL.[Line Discount Amount] / PIH.[Currency Factor]
    ELSE PIL.[Line Discount Amount]
    END AS DiscountAmount_LCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS CostAmountPostedToGL_LCY
  -- measures_RCY
  , PIL.[Unit Cost (LCY)] * PIL.Quantity * ER.CrossRate AS PurchaseAmount_RCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS PurchaseAmountExpected_RCY
  , CASE 
    WHEN PIH.[Currency Factor] <> 0
      THEN (PIL.[Line Discount Amount] / PIH.[Currency Factor]) * ER.CrossRate
    ELSE PIL.[Line Discount Amount] * ER.CrossRate
    END AS DiscountAmount_RCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS CostAmountPostedToGL_RCY
  -- measures_PCY
  , PIL.[Unit Cost (LCY)] * PIL.Quantity AS PurchaseAmount_PCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS PurchaseAmountExpected_PCY
  , CASE 
    WHEN PIH.[Currency Factor] <> 0
      THEN PIL.[Line Discount Amount] / PIH.[Currency Factor]
    ELSE PIL.[Line Discount Amount]
    END AS DiscountAmount_PCY
  , CAST(0.00 AS DECIMAL(38, 20)) AS CostAmountPostedToGL_PCY
FROM stage_nav.[Purch_ Inv_ Line] AS PIL
LEFT JOIN stage_nav.[Purch_ Inv_ Header] AS PIH
  ON PIL.[Document No_] = PIH.[No_]
    AND PIL.Company_Id = PIH.Company_ID
    AND PIL.Data_connection_id = PIH.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON PIL.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = PIL.company_id
    AND ER.DataConnectionID = PIL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON PIL.[Dimension Set ID] = FD.dimensionsetno
    AND PIL.company_id = FD.companyid
    AND PIL.data_connection_id = FD.DataConnectionID
WHERE PIL.Type NOT IN (
    2
    , 6
    ) -- Item, Itemcharge
