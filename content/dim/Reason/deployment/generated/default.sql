SELECT
  -- system information
  RC.stage_id AS StageID
  , RC.company_id AS CompanyID
  , RC.data_connection_id AS DataConnectionID
  -- business key
  , RC.Code AS ReasonCode
  --  attributes                        
  , COALESCE(NULLIF(RC.Description, ''), 'N/A') AS ReasonDescription
  , COALESCE(NULLIF(RC.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(RC.Description, ''), 'N/A') AS ReasonCodeDescription
FROM stage_nav.[Reason Code] AS RC
