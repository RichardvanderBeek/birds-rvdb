SELECT
  --   Business Key                                                                                       
  E_T.OptionNo AS JobLineTypeCode
  --  Attributes
  , COALESCE(NULLIF(E_T.OptionValue, ''), 'N/A') AS JobLineTypeDescription
  , E_T.DataConnectionId AS DataConnectionID
  , NS.table_id AS TableID
FROM help.Enumerations AS E_T
LEFT JOIN meta.nav_source_tables AS NS
  ON E_T.TableName = NS.table_name
    AND E_T.DataConnectionId = NS.data_connection_id
WHERE E_T.TableName IN (
    'Job Ledger Entry'
    , 'Job Planning Line'
    )
  AND E_T.ColumnName = 'Type'
