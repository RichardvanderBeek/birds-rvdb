EXEC dbo.drop_object @object = N'help.LoadCustomEnumerations', @type = N'P' ;
GO

CREATE PROCEDURE help.LoadCustomEnumerations
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

/****************************************************************************************************
  Remove all custom enumerations
  Date 		Changed by 	Ticket/Change 	Description
*****************************************************************************************************/

DELETE FROM help.Enumerations WHERE isCustom = 1 ;
SET @deleted = @@ROWCOUNT ;

/****************************************************************************************************
  Add custom enumerations here in this section
  Date 		Changed by 	Ticket/Change 	Description
*****************************************************************************************************/

/**
You can add enumerations by adding the following lins to this stored procedure
EXEC help.AddCustomEnumeration @table_name = 'some table', @column_name = 'some column' , @option_id= 0, @option_value = 'None'
EXEC help.AddCustomEnumeration @table_name = 'some table', @column_name = 'some column' , @option_id= 1, @option_value = 'Option 1'
EXEC help.AddCustomEnumeration @table_name = 'some table', @column_name = 'some column' , @option_id= 2, @option_value = 'Option 2'
**/
EXEC help.AddCustomEnumeration @table_name = 'Cust. Ledger Entry', @column_name = 'Open' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Cust. Ledger Entry', @column_name = 'Open' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Vendor Ledger Entry', @column_name = 'Open' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Vendor Ledger Entry', @column_name = 'Open' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;

EXEC help.AddCustomEnumeration @table_name = 'Fixed Asset', @column_name = 'Blocked' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Fixed Asset', @column_name = 'Blocked' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Fixed Asset', @column_name = 'Inactive' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Fixed Asset', @column_name = 'Inactive' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Fixed Asset', @column_name = 'Budgeted Asset' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Fixed Asset', @column_name = 'Budgeted Asset' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;

EXEC help.AddCustomEnumeration @table_name = 'Job', @column_name = 'Blocked' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Job', @column_name = 'Blocked' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;

EXEC help.AddCustomEnumeration @table_name = 'Project Cost Object', @column_name = 'Blocked' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Project Cost Object', @column_name = 'Blocked' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;

EXEC help.AddCustomEnumeration @table_name = 'Project Element', @column_name = 'Blocked' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Project Element', @column_name = 'Blocked' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;

EXEC help.AddCustomEnumeration @table_name = 'Work Order', @column_name = 'Firm Planned' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Work Order', @column_name = 'Firm Planned' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Work Order', @column_name = 'Signature Mandatory' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Work Order', @column_name = 'Signature Mandatory' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Work Order', @column_name = 'Finished' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Work Order', @column_name = 'Finished' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;

EXEC help.AddCustomEnumeration @table_name = 'Type', @column_name = 'Account Type' , @option_id= 1, @option_value = 'GL Account' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Type', @column_name = 'Account Type' , @option_id= 2, @option_value = 'Item' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Type', @column_name = 'Account Type' , @option_id= 3, @option_value = 'Resource' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Type', @column_name = 'Account Type' , @option_id= 4, @option_value = 'Fixed Asset' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Type', @column_name = 'Account Type' , @option_id= 5, @option_value = 'Service Cost' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Type', @column_name = 'Account Type' , @option_id= 6, @option_value = 'Item Charge' , @component_execution_id = @component_execution_id;

EXEC help.AddCustomEnumeration @table_name = 'Plant Kind', @column_name = 'Equipment Kind' , @option_id= 0, @option_value = 'Materieel' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Plant Kind', @column_name = 'Equipment Kind' , @option_id= 1, @option_value = 'Artikel' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Plant Kind', @column_name = 'Equipment Kind' , @option_id= 2, @option_value = 'Tekst' , @component_execution_id = @component_execution_id;

EXEC help.AddCustomEnumeration @table_name = 'Plant Number', @column_name = 'External' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Plant Number', @column_name = 'External' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;

EXEC help.AddCustomEnumeration @table_name = 'Lot No_ Information', @column_name = 'Blocked' , @option_id= 0, @option_value = 'No' , @component_execution_id = @component_execution_id;
EXEC help.AddCustomEnumeration @table_name = 'Lot No_ Information', @column_name = 'Blocked' , @option_id= 1, @option_value = 'Yes' , @component_execution_id = @component_execution_id;

END;


