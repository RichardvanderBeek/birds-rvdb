SELECT
  --   system information
  JLE.stage_id AS StageID
  , JLE.company_id AS CompanyID
  , JLE.data_connection_id AS DataConnectionID
  -- dimensions       
  , JLE.[Entry No_] AS EntryCode
  , JLE.[Entry Type] AS EntryType
  , JLE.[Job No_] AS JobCode
  , JLE.[Job Task No_] AS JobTaskCode
  , JLE.[Posting Date] AS PostingDate
  , CASE 
    WHEN JLE.Type = 0
      THEN JLE.No_
    ELSE NULL
    END AS ResourceCode
  , JLE.[Resource Group No_] AS ResourceGroupCode
  , CASE 
    WHEN JLE.Type = 1
      THEN JLE.No_
    ELSE NULL
    END AS ItemCode
  , CASE 
    WHEN JLE.Type = 2
      THEN JLE.No_
    ELSE NULL
    END AS GeneralLedgerAccountCode
  , JLE.Type AS JobLineTypeCode
  , JB.[Bill-to Customer No_] AS BillToCustomerCode
  , JLE.[Unit of Measure Code] AS UnitOfMeasureCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  , 169 AS TableID --Job Ledger Entry                      
  -- measures
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 0
      THEN JLE.Quantity
    ELSE 0
    END AS HourConsumption -- Usage, Resource
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 1
      THEN JLE.Quantity
    ELSE 0
    END AS ItemQuantity -- Usage, Item
  -- LCY          
  -- Resource         
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 0
      THEN JLE.[Total Cost (LCY)]
    ELSE 0
    END AS ResourceCost_LCY -- Usage, Resource
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 0
      THEN JLE.[Total Price (LCY)]
    ELSE 0
    END AS ResourceSalesExpected_LCY -- Usage, Resource
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 0
      THEN JLE.[Total Price (LCY)] * - 1
    ELSE 0
    END AS ResourceSalesActual_LCY -- Sales, Resource
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 0
      THEN JLE.[Line Discount Amount (LCY)] * - 1
    ELSE 0
    END AS ResourceDiscount_LCY -- Discount Amount, Resource
  -- Item          
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 1
      THEN JLE.[Total Cost (LCY)]
    ELSE 0
    END AS ItemCost_LCY -- Usage, Item
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 1
      THEN JLE.[Total Price (LCY)]
    ELSE 0
    END AS ItemSalesExpected_LCY -- Usage, Item
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 1
      THEN JLE.[Total Price (LCY)] * - 1
    ELSE 0
    END AS ItemSalesActual_LCY -- Sales, Item
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 1
      THEN JLE.[Line Discount Amount (LCY)] * - 1
    ELSE 0
    END AS ItemDiscount_LCY -- Discount Amount, Item
  -- G/L Account         
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 2
      THEN JLE.[Total Cost (LCY)]
    ELSE 0
    END AS GLCost_LCY -- Usage, G/L Account
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 2
      THEN JLE.[Total Price (LCY)] * - 1
    ELSE 0
    END AS GLSales_LCY -- Sales, G/L Account
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 2
      THEN JLE.[Line Discount Amount (LCY)] * - 1
    ELSE 0
    END AS GLDiscount_LCY -- Discount Amount, G/L Amount
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 2
      THEN JLE.[Total Price (LCY)]
    ELSE 0
    END AS GLSalesExpected_LCY -- Usage, G/L Account
  -- PCY          
  -- Resource         
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 0
      THEN JLE.[Total Cost]
    ELSE 0
    END AS ResourceCost_PCY -- Usage, Resource
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 0
      THEN JLE.[Total Price]
    ELSE 0
    END AS ResourceSalesExpected_PCY -- Usage, Resource
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 0
      THEN JLE.[Total Price] * - 1
    ELSE 0
    END AS ResourceSalesActual_PCY -- Sales, Resource
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 0
      THEN JLE.[Line Discount Amount] * - 1
    ELSE 0
    END AS ResourceDiscount_PCY -- Discount Amount, Resource
  -- Item                
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 1
      THEN JLE.[Total Cost]
    ELSE 0
    END AS ItemCost_PCY -- Usage, Item
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 1
      THEN JLE.[Total Price]
    ELSE 0
    END AS ItemSalesExpected_PCY -- Usage, Item
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 1
      THEN JLE.[Total Price] * - 1
    ELSE 0
    END AS ItemSalesActual_PCY -- Sales, Item
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 1
      THEN JLE.[Line Discount Amount] * - 1
    ELSE 0
    END AS ItemDiscount_PCY -- Discount Amount, Item
  -- G/L Account               
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 2
      THEN JLE.[Total Cost]
    ELSE 0
    END AS GLCost_PCY -- Usage, G/L Account
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 2
      THEN JLE.[Total Price] * - 1
    ELSE 0
    END AS GLSales_PCY -- Sales, G/L Account
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 2
      THEN JLE.[Line Discount Amount] * - 1
    ELSE 0
    END AS GLDiscount_PCY -- Discount Amount, G/L Amount
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 2
      THEN JLE.[Total Price]
    ELSE 0
    END AS GLSalesExpected_PCY -- Usage, G/L Account
  -- RCY
  -- Resource
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 0
      THEN JLE.[Total Cost] * ER.CrossRate
    ELSE 0
    END AS ResourceCost_RCY -- Usage, Resource
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 0
      THEN JLE.[Total Price] * ER.CrossRate
    ELSE 0
    END AS ResourceSalesExpected_RCY -- Usage, Resource
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 0
      THEN JLE.[Total Price] * - 1 * ER.CrossRate
    ELSE 0
    END AS ResourceSalesActual_RCY -- Sales, Resource
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 0
      THEN JLE.[Line Discount Amount] * - 1 * ER.CrossRate
    ELSE 0
    END AS ResourceDiscount_RCY -- Discount Amount, Resource
  -- Item 
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 1
      THEN JLE.[Total Cost] * ER.CrossRate
    ELSE 0
    END AS ItemCost_RCY -- Usage, Item
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 1
      THEN JLE.[Total Price] * ER.CrossRate
    ELSE 0
    END AS ItemSalesExpected_RCY -- Usage, Item
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 1
      THEN JLE.[Total Price] * - 1 * ER.CrossRate
    ELSE 0
    END AS ItemSalesActual_RCY -- Sales, Item
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 1
      THEN JLE.[Line Discount Amount] * - 1 * ER.CrossRate
    ELSE 0
    END AS ItemDiscount_RCY -- Discount Amount, Item
  -- G/L Account
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 2
      THEN JLE.[Total Cost] * ER.CrossRate
    ELSE 0
    END AS GLCost_RCY -- Usage, G/L Account
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 2
      THEN JLE.[Total Price] * - 1 * ER.CrossRate
    ELSE 0
    END AS GLSales_RCY -- Sales, G/L Account
  , CASE 
    WHEN JLE.[Entry Type] = 1
      AND JLE.Type = 2
      THEN JLE.[Line Discount Amount] * - 1 * ER.CrossRate
    ELSE 0
    END AS GLDiscount_RCY -- Discount Amount, G/L Amount
  , CASE 
    WHEN JLE.[Entry Type] = 0
      AND JLE.Type = 2
      THEN JLE.[Total Price] * ER.CrossRate
    ELSE 0
    END AS GLSalesExpected_RCY -- Usage, G/L Account
FROM stage_nav.[Job Ledger Entry] AS JLE
LEFT JOIN help.ExchangeRates AS ER
  ON JLE.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = JLE.company_id
    AND ER.DataConnectionID = JLE.data_connection_id
LEFT JOIN stage_nav.Job AS JB
  ON JB.No_ = JLE.[Job No_]
    AND ER.CompanyId = JLE.company_id
    AND ER.DataConnectionID = JLE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON JLE.[Dimension Set ID] = FD.DimensionSetNo
    AND JLE.company_id = FD.CompanyID
    AND JLE.data_connection_id = FD.DataConnectionID
