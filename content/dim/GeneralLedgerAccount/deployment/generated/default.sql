SELECT
  --   System information
  GLA.stage_id AS StageID
  , GLA.company_id AS CompanyID
  , GLA.data_connection_id AS DataConnectionID
  --    Business key
  , GLA.No_ AS GeneralLedgerAccountCode
  --  Attributes
  , COALESCE(NULLIF(GLA.NAME, ''), 'N/A') AS GeneralLedgerAccountName
  , COALESCE(NULLIF(GLA.No_, ''), 'N/A') + ' - ' + COALESCE(NULLIF(GLA.NAME, ''), 'N/A') AS GeneralLedgerAccountCodeName
  , COALESCE(GLA.[Account Type], - 1) AS GeneralLedgerAccountType
  , COALESCE(NULLIF(e_AT.OptionValue, ''), 'N/A') AS GeneralLedgerAccountTypeDescription
  , COALESCE(GLA.[Account Category], - 1) AS AccountCategory
  , COALESCE(NULLIF(e_GLA.OptionValue, ''), 'N/A') AS AccountCategoryDescription
  , COALESCE(GLA.Income_Balance, - 1) AS IncomeBalance
  , COALESCE(NULLIF(e_IB.OptionValue, ''), 'N/A') AS IncomeBalanceDescription
  , COALESCE(NULLIF(GLH.LevelCode0, ''), 'None') AS LevelCode0
  , COALESCE(NULLIF(GLH.LevelName0, ''), 'None') AS LevelName0
  , COALESCE(NULLIF(GLH.LevelCode0, ''), 'None') + ' - ' + COALESCE(NULLIF(GLH.LevelName0, ''), 'None') AS LevelCodeName0
  , COALESCE(NULLIF(GLH.LevelCode1, ''), 'None') AS LevelCode1
  , COALESCE(NULLIF(GLH.LevelName1, ''), 'None') AS LevelName1
  , COALESCE(NULLIF(GLH.LevelCode1, ''), 'None') + ' - ' + COALESCE(NULLIF(GLH.LevelName1, ''), 'None') AS LevelCodeName1
  , COALESCE(NULLIF(GLH.LevelCode2, ''), 'None') AS LevelCode2
  , COALESCE(NULLIF(GLH.LevelName2, ''), 'None') AS LevelName2
  , COALESCE(NULLIF(GLH.LevelCode2, ''), 'None') + ' - ' + COALESCE(NULLIF(GLH.LevelName2, ''), 'None') AS LevelCodeName2
  , COALESCE(NULLIF(GLH.LevelCode3, ''), 'None') AS LevelCode3
  , COALESCE(NULLIF(GLH.LevelName3, ''), 'None') AS LevelName3
  , COALESCE(NULLIF(GLH.LevelCode3, ''), 'None') + ' - ' + COALESCE(NULLIF(GLH.LevelName3, ''), 'None') AS LevelCodeName3
  , COALESCE(NULLIF(GLH.LevelCode4, ''), 'None') AS LevelCode4
  , COALESCE(NULLIF(GLH.LevelName4, ''), 'None') AS LevelName4
  , COALESCE(NULLIF(GLH.LevelCode4, ''), 'None') + ' - ' + COALESCE(NULLIF(GLH.LevelName4, ''), 'None') AS LevelCodeName4
  --Imported GL Account Hierarchy
  , ISNULL(NULLIF(GLAH.AccountType, ''), 'N/A') AS GLHAccountType
  , ISNULL(NULLIF(GLAH.Level1Account, ''), 'N/A') AS GLHLevel1Account
  , ISNULL(NULLIF(GLAH.Level1Desc, ''), 'N/A') AS GLHLevel1Desc
  , ISNULL(NULLIF(GLAH.Level1Account, '') + ' - ', '') + ISNULL(NULLIF(GLAH.Level1Desc, ''), 'N/A') AS GLHLevel1AccountDesc
  , ISNULL(NULLIF(GLAH.Level2Account, ''), 'N/A') AS GLHLevel2Account
  , ISNULL(NULLIF(GLAH.Level2Desc, ''), 'N/A') AS GLHLevel2Desc
  , ISNULL(NULLIF(GLAH.Level2Account, '') + ' - ', '') + ISNULL(NULLIF(GLAH.Level2Desc, ''), 'N/A') AS GLHLevel2AccountDesc
  , ISNULL(NULLIF(GLAH.Level3Account, ''), 'N/A') AS GLHLevel3Account
  , ISNULL(NULLIF(GLAH.Level3Desc, ''), 'N/A') AS GLHLevel3Desc
  , ISNULL(NULLIF(GLAH.Level3Account, '') + ' - ', '') + ISNULL(NULLIF(GLAH.Level3Desc, ''), 'N/A') AS GLHLevel3AccountDesc
  , ISNULL(NULLIF(GLAH.Level4Account, ''), 'N/A') AS GLHLevel4Account
  , ISNULL(NULLIF(GLAH.Level4Desc, ''), 'N/A') AS GLHLevel4Desc
  , ISNULL(NULLIF(GLAH.Level4Account, '') + ' - ', '') + ISNULL(NULLIF(GLAH.Level4Desc, ''), 'N/A') AS GLHLevel4AccountDesc
  , ISNULL(NULLIF(GLAH.Level5Account, ''), 'N/A') AS GLHLevel5Account
  , ISNULL(NULLIF(GLAH.Level5Desc, ''), 'N/A') AS GLHLevel5Desc
  , ISNULL(NULLIF(GLAH.Level5Account, '') + ' - ', '') + ISNULL(NULLIF(GLAH.Level5Desc, ''), 'N/A') AS GLHLevel5AccountDesc
  , ISNULL(NULLIF(GLAH.Level6Account, ''), 'N/A') AS GLHLevel6Account
  , ISNULL(NULLIF(GLAH.Level6Desc, ''), 'N/A') AS GLHLevel6Desc
  , ISNULL(NULLIF(GLAH.Level6Account, '') + ' - ', '') + ISNULL(NULLIF(GLAH.Level6Desc, ''), 'N/A') AS GLHLevel6AccountDesc
  , ISNULL(NULLIF(GLAH.Level7Account, ''), 'N/A') AS GLHLevel7Account
  , ISNULL(NULLIF(GLAH.Level7Desc, ''), 'N/A') AS GLHLevel7Desc
  , ISNULL(NULLIF(GLAH.Level7Account, '') + ' - ', '') + ISNULL(NULLIF(GLAH.Level7Desc, ''), 'N/A') AS GLHLevel7AccountDesc
  , ISNULL(NULLIF(GLAH.Level8Account, ''), 'N/A') AS GLHLevel8Account
  , ISNULL(NULLIF(GLAH.Level8Desc, ''), 'N/A') AS GLHLevel8Desc
  , ISNULL(NULLIF(GLAH.Level8Account, '') + ' - ', '') + ISNULL(NULLIF(GLAH.Level8Desc, ''), 'N/A') AS GLHLevel8AccountDesc
  , ISNULL(NULLIF(GLAH.GLAccount, ''), 'N/A') AS GLHGLAccount
  , ISNULL(NULLIF(GLAH.GLAccountDesc, ''), 'N/A') AS GLHGLAccountDesc
  , ISNULL(NULLIF(GLAH.GLAccount, '') + ' - ', '') + ISNULL(NULLIF(GLAH.GLAccountDesc, ''), 'N/A') AS GLHGLAccountAccountDesc
FROM stage_nav.[G_L Account] AS GLA
LEFT JOIN help.GeneralLedgerHierarchies AS GLH
  ON GLA.No_ = GLH.Code
    AND GLA.company_id = GLH.CompanyID
    AND GLA.data_connection_id = GLH.DataConnectionID
LEFT JOIN help.Enumerations AS e_GLA
  ON e_GLA.TableName = 'G/L Account'
    AND e_GLA.ColumnName = 'Account Category'
    AND e_GLA.OptionNo = GLA.[Account Category]
    AND e_GLA.DataConnectionId = GLA.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_AT
  ON e_AT.TableName = 'G/L Account'
    AND e_AT.ColumnName = 'Account Type'
    AND e_AT.OptionNo = GLA.[Account Type]
    AND e_AT.DataConnectionId = GLA.Data_Connection_Id
LEFT JOIN help.Enumerations AS e_IB
  ON e_IB.TableName = 'G/L Account'
    AND e_IB.ColumnName = 'Income/Balance'
    AND e_IB.OptionNo = GLA.Income_Balance
    AND e_IB.DataConnectionId = GLA.Data_Connection_Id
--Imported GL Account Hierarchy
LEFT JOIN help.GLAccountHierarchy AS GLAH
  ON GLAH.GLAccount = GLA.[No_]
    AND GLAH.DataConnectionID = GLA.data_connection_id
