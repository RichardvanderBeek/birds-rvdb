SELECT
  -- System information
  DP.StageID AS StageID
  , DP.CompanyID AS CompanyID
  , DP.DataConnectionID AS DataConnectionID
  -- Business Key                    
  , DP.DocumentNo AS DocumentNo
  , DP.[DocumentType] AS DocumentTypeCode
  -- Attribute
  , COALESCE(e_dt.OptionValue, 'N/A') AS DocumentTypeDescription
FROM (
  SELECT DISTINCT PRH.stage_id AS StageID
    , PRH.company_id AS CompanyID
    , PRH.data_connection_id AS DataConnectionID
    , PRH.[Posting Date] AS PostingDate
    , 5 AS DocumentType --Purchase Receipt
    , PRH.[No_] AS DocumentNo
  FROM stage_nav.[Purch_ Rcpt_ Header] AS PRH
  
  UNION ALL
  
  SELECT DISTINCT PIH.stage_id AS StageID
    , PIH.company_id AS CompanyID
    , PIH.data_connection_id AS DataConnectionID
    , PIH.[Posting Date] AS PostingDate
    , 6 AS DocumentType --Purchase Invoice
    , PIH.[No_] AS DocumentNo
  FROM stage_nav.[Purch_ Inv_ Header] AS PIH
  
  UNION ALL
  
  SELECT DISTINCT RRH.stage_id AS StageID
    , RRH.company_id AS CompanyID
    , RRH.data_connection_id AS DataConnectionID
    , RRH.[Posting Date] AS PostingDate
    , 7 AS DocumentType --Return Receipt
    , RRH.[No_] AS DocumentNo
  FROM stage_nav.[Return Receipt Header] AS RRH
  
  UNION ALL
  
  SELECT DISTINCT PCMH.stage_id AS StageID
    , PCMH.company_id AS CompanyID
    , PCMH.data_connection_id AS DataConnectionID
    , PCMH.[Posting Date] AS PostingDate
    , 8 AS DocumentType --Purchase Credit Memo
    , PCMH.[No_] AS DocumentNo
  FROM stage_nav.[Purch_ Cr_ Memo Hdr_] AS PCMH
  ) DP
LEFT JOIN help.Enumerations AS e_dt
  ON e_dt.TableName = 'Item Ledger Entry'
    AND e_dt.ColumnName = 'Document Type'
    AND e_dt.OptionNo = DP.[DocumentType]
    AND e_dt.DataConnectionId = DP.DataConnectionID
