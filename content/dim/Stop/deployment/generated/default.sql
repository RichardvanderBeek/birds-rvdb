SELECT
  -- system information
  S.stage_id AS StageID
  , S.company_id AS CompanyID
  , S.data_connection_id AS DataConnectionID
  -- business key
  , S.Code AS StopCode
  --  attributes
  , S.Description AS StopDescription
  , S.Code + ISNULL(' - ' + NULLIF(S.Description, ''), '') AS StopCodeDescription
FROM stage_nav.[Stop] AS S
