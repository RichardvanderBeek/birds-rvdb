SELECT
  -- system information
  PO.stage_id AS StageID
  , PO.company_id AS CompanyID
  , PO.data_connection_id AS DataConnectionID
  -- business key
  , PO.[No_] AS ProductionOrderCode
  --  attributes
  , COALESCE(NULLIF(PO.Description, ''), 'N/A') AS ProductionOrderDescription
  , COALESCE(NULLIF(PO.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(PO.Description, ''), 'N/A') AS ProductionOrderCodeDescription
  , PO.[Status] AS ProductionOrderStatusCode
  , COALESCE(NULLIF(EN.OptionValue, ''), 'N/A') AS ProductionOrderStatusDescription
FROM stage_nav.[Production Order] AS PO
LEFT JOIN help.Enumerations AS EN
  ON EN.OptionNo = PO.[Status]
    AND EN.ColumnName = 'Production Order'
    AND EN.TableName = 'Status'
    AND EN.DataConnectionID = PO.data_connection_id
