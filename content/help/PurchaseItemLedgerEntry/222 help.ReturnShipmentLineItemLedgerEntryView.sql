/***********************************************************************************
Functionality:  This SQL script creates the help.ReturnShipmentLineItemLedgerEntryView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.ReturnShipmentLineItemLedgerEntryView', @type = N'V' ;
GO
CREATE VIEW help.ReturnShipmentLineItemLedgerEntryView
AS
  SELECT
    -- system information
                    RRL.stage_id                                     AS StageID
                  , RRL.company_id                                   AS CompanyID
                  , RRL.data_connection_id                           AS DataConnectionID
                  -- business key
                  , ILE.[Entry No_]                                  AS EntryNo
                  , ILE.[Document Type]                              AS DocumentType
                  , ILE.[Document No_]                               AS DocumentNo
                  , ILE.[Posting Date]                               AS PostingDate
                  , COALESCE(RRH.[Posting Date], RRL.[Posting Date]) AS DocumentPostingDate
                  , RRL.[Buy-from Vendor No_]                        AS BuyFromVendorNo
                  , RRL.[Pay-to Vendor No_]                          AS PayToVendorNo
    FROM            stage_nav.[Return Shipment Line]   AS RRL
    LEFT OUTER JOIN stage_nav.[Return Shipment Header] AS RRH ON RRL.[Document No_]     = RRH.[No_]
                                                             AND RRL.company_id         = RRH.company_id
                                                             AND RRL.data_connection_id = RRH.data_connection_id
   INNER JOIN       stage_nav.[Item Ledger Entry]      AS ILE ON ILE.[Document Type]    = 7 --Return Shipment
                                                             AND RRL.[Document No_]     = ILE.[Document No_]
                                                             AND RRL.[Line No_]         = ILE.[Document Line No_]
                                                             AND ILE.[Entry Type]       = 0 --Purchase  
                                                             AND RRL.company_id         = ILE.company_id
                                                             AND RRL.data_connection_id = ILE.data_connection_id
   WHERE            RRL.execution_flag <> 'D'
     AND            RRH.execution_flag            <> 'D'
     AND            ILE.execution_flag            <> 'D' ;
GO
