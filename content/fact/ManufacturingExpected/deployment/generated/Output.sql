SELECT
  -- system information
  POL.stage_id AS StageID
  , POL.company_id AS CompanyID
  , POL.data_connection_id AS DataConnectionID
  , 3 AS ManufacturingExpectedType -- Output
  -- dimensions       
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  , POL.[Prod_ Order No_] AS ProductionOrderCode
  , POL.[Location Code] AS LocationCode
  , POL.[Item No_] AS ItemCode
  , POL.[Variant Code] AS ItemVariantCode
  , POL.[Item No_] AS OutputItemCode
  , POL.STATUS AS StatusCode
  , 3 AS SourceType -- Work Center = 0, Machine Center = 1, Item = 3
  , 6 AS EntryType --
  , POL.[Due Date] AS [Date]
  -- measures
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedConsumptionQuantity
  , POL.[Quantity (Base)] AS ExpectedTotalQuantity
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedConsumptionCost
  , POL.[Unit Cost] * POL.[Quantity (Base)] AS ExpectedTotalCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedOverheadCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedCapacityNeed
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedCapacityOverheadCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedRunTime
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedSetupTime
  , POL.[Unit Cost] * POL.[Quantity (Base)] AS ExpectedOutputCost
  , POL.[Quantity (Base)] AS ExpectedOutputQuantity
FROM stage_nav.[Prod_ Order Line] AS POL
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON POL.[Dimension Set ID] = FD.DimensionSetNo
    AND POL.company_id = FD.CompanyID
    AND POL.data_connection_id = FD.DataConnectionID
