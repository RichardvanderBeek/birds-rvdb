SELECT
  -- system information
  BM.stage_id AS StageID
  , BM.company_id AS CompanyID
  , BM.data_connection_id AS DataConnectionID
  -- business key
  , BM.NAME AS BudgetModelName
  --  attributes                        
  , COALESCE(NULLIF(BM.Description, ''), 'N/A') AS BudgetModelDescription
  , COALESCE(NULLIF(BM.NAME, ''), 'N/A') + ' - ' + COALESCE(NULLIF(BM.Description, ''), 'N/A') AS BudgetModelNameDescription
FROM stage_nav.[G_L Budget Name] AS BM
