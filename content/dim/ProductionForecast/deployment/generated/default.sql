SELECT
  -- System Information
  PF.stage_id AS StageID
  , PF.company_id AS CompanyID
  , PF.data_connection_id AS DataConnectionID
  -- Business Key                                                                                      
  , PF.[Name] AS ProductionForecastCode
  -- Attributes
  , COALESCE(ISNULL(PF.[Name], ''), 'N/A') AS ProductionForecastName
  , COALESCE(ISNULL(PF.[Name], ''), 'N/A') + ' - ' + COALESCE(ISNULL(PF.Description, ''), 'N/A') AS ProductionForecastCodeName
FROM stage_nav.[Production Forecast Name] AS PF
