SELECT
  -- System Information
  PG.stage_id AS StageID
  , PG.company_id AS CompanyID
  , PG.data_connection_id AS DataConnectionID
  -- Business Key                                                                                       
  , PG.Code AS PostingGroupCode
  , PG.Code AS PostingGroupDesc
FROM stage_nav.[FA Posting Group] AS PG
