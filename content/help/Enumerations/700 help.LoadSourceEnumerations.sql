/*************************************************************************************
Functionality:   This SQL script Loads the view "dim.EnumerationsView" into the table "dim.Enumerations"
Created by:      Jeroen van Lier    Date:              16-01-2019

Date            Changed By        Ticket/Change      Description

**************************************************************************************/
EXEC dbo.drop_object @object = N'help.LoadSourceEnumerations', @type = N'P' ;
GO
CREATE PROCEDURE help.LoadSourceEnumerations
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN
  SET @deleted = 0 ;
  SET @updated = 0 ;
  SET @inserted = 0 ;
  UPDATE      help.Enumerations
     SET
    -- System information
              ComponentExecutionId = @component_execution_id
            -- Attributes
            , OptionValue = EV.OptionValue
    FROM      help.EnumerationsView AS EV
   INNER JOIN help.Enumerations     AS E ON EV.TableName        = E.TableName
                                        AND EV.ColumnName       = E.ColumnName
                                        AND EV.OptionNo         = E.OptionNo
                                        AND EV.DataConnectionId = E.DataConnectionId ;
  SELECT @updated = @@ROWCOUNT ;

  INSERT INTO help.Enumerations (
    ComponentExecutionId
  , DataConnectionId
  , TableName
  , ColumnName
  , OptionNo
  , OptionValue
  , IsCustom
  )
  SELECT @component_execution_id
       , EV.DataConnectionId
       , EV.TableName
       , EV.ColumnName
       , EV.OptionNo
       , EV.OptionValue
       , 0
    FROM help.EnumerationsView AS EV
   WHERE NOT EXISTS ( SELECT *
                        FROM help.Enumerations AS E
                       WHERE EV.TableName        = E.TableName
                         AND EV.ColumnName       = E.ColumnName
                         AND EV.OptionNo         = E.OptionNo
                         AND EV.DataConnectionId = E.DataConnectionId) ;
  SELECT @inserted = @@ROWCOUNT ;
END ;
GO