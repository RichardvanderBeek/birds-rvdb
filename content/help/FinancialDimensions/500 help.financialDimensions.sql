EXEC dbo.drop_object @object = N'help.FinancialDimensions', @type = N'T' ;
GO
CREATE TABLE help.FinancialDimensions
(
  ComponentExecutionId    INT           NOT NULL
, CompanyId               INT           NOT NULL
, DataConnectionId        INT           NOT NULL
, FinancialDimensionsId   INT           NOT NULL
, DimensionCode           NVARCHAR(30)  NOT NULL
, DimensionGlobalSequence INT           NOT NULL
, DimensionSetNo          INT           NOT NULL
, DimensionValueNo        INT           NOT NULL
, DimensionValueCode      NVARCHAR(30)  NOT NULL
, DimensionValueName      NVARCHAR(100) NOT NULL
, DimensionValueCodeName  NVARCHAR(255) NOT NULL
) ;