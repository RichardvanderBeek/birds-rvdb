SELECT
  -- System Information
  Jo.stage_id AS StageID
  , Jo.company_id AS CompanyID
  , Jo.data_connection_id AS DataConnectionID
  -- Business Key                                                                                                                                 
  , Jo.No_ AS JobCode
  -- Attributes                                                                                                                                   
  , COALESCE(NULLIF(Jo.No_ + ' ' + Jo.Description, ''), 'N/A') AS ProjectName
  , COALESCE(NULLIF(Jo.Description, ''), 'N/A') AS ProjectDescription
  , COALESCE(NULLIF(Jo.[Description 2], ''), 'N/A') AS ProjectDescription2
  , COALESCE(NULLIF(Jo.[Starting Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS StartingDate
  , COALESCE(NULLIF(Jo.[Creation Date], '1753-01-01 00:00:00'), '1900-01-01 00:00:000') AS CreationDate
  , COALESCE(NULLIF(Jo.[Ending Date], '1753-01-01 00:00:00'), '9999-12-31 00:00:000') AS EndingDate
  , COALESCE(NULLIF(Jo.[Bill-To Customer No_], ''), 'N/A') AS BilltoCustomerNo
  , COALESCE(NULLIF(Jo.[Bill-To Customer No_] + ' - ' + c.NAME, ''), 'N/A') AS BilltoCustomerNoName
  , COALESCE(NULLIF(Jo.[Person Responsible], ''), 'N/A') + ' - ' + COALESCE(NULLIF(COALESCE(NULLIF(EmPR.[First Name], '') + ' ', '') + COALESCE(NULLIF(EmPR.[Middle Name], '') + ' ', '') + COALESCE(NULLIF(EmPR.[Last Name], ''), ''), ''), 'N/A') AS PersonResponsible
  , Jo.Blocked AS BlockedCode
  , COALESCE(e_b1.OptionValue, 'N/A') AS BlockedDescription
  , COALESCE(NULLIF(Jo.[Global Dimension 1 Code], ''), 'N/A') AS GlobalDimension1Code
  , COALESCE(NULLIF(Jo.[Global Dimension 2 Code], ''), 'N/A') AS GlobalDimension2Code
  , COALESCE(NULLIF(Jo.[Job Posting Group], ''), 'N/A') AS JobPostingGroup
  , COALESCE(NULLIF(Jo.[Search Description], ''), 'N/A') AS SearchDescription
  , Jo.STATUS AS JobStatusCode
  , COALESCE(e_JS.OptionValue, 'N/A') AS JobStatusDescription
FROM stage_nav.Job AS Jo
LEFT JOIN stage_nav.Customer AS c
  ON Jo.[Bill-To Customer No_] = c.No_
    AND Jo.data_connection_id = c.data_connection_id
    AND Jo.company_id = c.company_id
LEFT JOIN stage_nav.Employee AS EmPR
  ON EmPR.No_ = Jo.[Person Responsible]
    AND EmPR.data_connection_id = Jo.data_connection_id
    AND EmPR.company_id = Jo.company_id
-- JobStatus
LEFT JOIN help.Enumerations AS e_JS
  ON e_JS.TableName = 'Job'
    AND e_JS.ColumnName = 'Status'
    AND e_JS.OptionNo = Jo.STATUS
    AND e_JS.DataConnectionId = Jo.Data_Connection_Id
-- Blocked  
LEFT JOIN help.Enumerations AS e_b1
  ON e_b1.TableName = 'Job'
    AND e_b1.ColumnName = 'Blocked'
    AND e_b1.OptionNo = Jo.Blocked
    AND e_b1.DataConnectionId = Jo.Data_Connection_Id
