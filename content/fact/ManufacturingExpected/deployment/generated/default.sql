SELECT
  -- system information
  POC.stage_id AS StageID
  , POC.company_id AS CompanyID
  , POC.data_connection_id AS DataConnectionID
  , 1 AS ManufacturingExpectedType -- Material
  -- dimensions 
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  , POC.[Prod_ Order No_] AS ProductionOrderCode
  , POC.[Location Code] AS LocationCode
  , POC.[Item No_] AS ItemCode
  , POL.[Variant Code] AS ItemVariantCode
  , POL.[Item No_] AS OutputItemCode
  , POL.STATUS AS StatusCode
  , 3 AS SourceType -- Item = 3
  , 5 AS EntryType --
  , POC.[Due Date] AS [Date]
  -- measures
  , POC.[Expected Quantity] AS ExpectedConsumptionQuantity
  , POC.[Expected Quantity] AS ExpectedTotalQuantity
  , (POC.[Cost Amount] - POC.[Overhead Amount]) AS ExpectedConsumptionCost
  , (POC.[Cost Amount] + POC.[Overhead Amount]) AS ExpectedTotalCost
  , POC.[Overhead Amount] AS ExpectedOverheadCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedCapacityNeed
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedCapacityOverheadCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedRunTime
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedSetupTime
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedOutputCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedOutputQuantity
FROM stage_nav.[Prod_ Order Component] AS POC
LEFT JOIN stage_nav.[Prod_ Order Line] AS POL
  ON POC.[Prod_ Order No_] = POL.[Prod_ Order No_]
    AND POC.[Prod_ Order Line No_] = POL.[Line No_]
    AND POC.company_id = POL.company_id
    AND POC.data_connection_id = POL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON POC.[Dimension Set ID] = FD.DimensionSetNo
    AND POC.company_id = FD.CompanyID
    AND POC.data_connection_id = FD.DataConnectionID
