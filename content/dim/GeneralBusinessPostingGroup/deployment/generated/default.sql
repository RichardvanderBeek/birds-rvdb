SELECT
  -- System Information
  GBPG.stage_id AS StageID
  , GBPG.company_id AS CompanyID
  , GBPG.data_connection_id AS DataConnectionID
  -- Business Key
  , GBPG.Code AS GeneralBusinessPostingGroupCode
  -- Attributes
  , COALESCE(NULLIF(GBPG.Description, ''), 'N/A') AS GeneralBusinessPostingGroupDescription
  , COALESCE(NULLIF(GBPG.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(GBPG.Description, ''), 'N/A') AS GeneralBusinessPostingGroupCodeDescription
FROM stage_nav.[Gen_ Business Posting Group] AS GBPG
