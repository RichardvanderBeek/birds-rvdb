SELECT
  -- system information
  PSL.stage_id AS StageID
  , PSL.Company_Id AS CompanyID
  , PSL.data_connection_id AS DataConnectionID
  -- dimensions
  , PSH.No_ AS DocumentPurchaseCode
  , 5 AS DocumentPurchaseTypeCode -- Purchase Receipt
  , COALESCE(PSL.[Buy-From Vendor No_], PSH.[Buy-From Vendor No_]) AS BuyFromVendorCode
  , COALESCE(PSL.[Pay-to Vendor No_], PSL.[Pay-to Vendor No_]) AS PayToVendorCode
  , PSL.[Document No_] AS DocumentCode
  , 1 AS DocumentTypeCode
  , PSL.[No_] AS ItemCode
  , PSL.[Variant Code] AS ItemVariantCode
  , PSH.[Source Code] AS SourceCode
  , COALESCE(PSL.[Location Code], PSH.[Location Code]) AS LocationCode
  , PSL.[Return Reason Code] AS ReturnReasonCode
  , PSL.[Gen_ Prod_ Posting Group] AS GeneralProductPostingGroupCode
  , COALESCE(PSL.[Gen_ Bus_ Posting Group], PSH.[Gen_ Bus_ Posting Group]) AS GeneralBusinessPostingGroupCode
  , PSL.[Unit of Measure Code] AS UnitOfMeasureCode
  , PSH.[Reason Code] AS ReasonCode
  , PSH.[Purchaser Code] AS SalesPersonPurchaserCode
  , PSH.[Shipment Method Code] AS ShipmentMethodCode
  , COALESCE(PSL.[Transport Method], PSH.[Transport Method]) AS TransportMethodCode
  , PSH.[Order No_] AS PurchaseOrderCode
  , PSL.[Posting Date] AS PostingDate
  , COALESCE(PSL.[Order Date], PSH.[Order Date]) AS OrderDate
  , COALESCE(PSL.[Requested Receipt Date], PSH.[Requested Receipt Date]) AS RequestedReceiptDate
  , PSL.[Expected Receipt Date] AS ExpectedReceiptDate
  , COALESCE(PSL.[Promised Receipt Date], PSH.[Promised Receipt Date]) AS PromisedReceiptDate
  , PSL.[Planned Receipt Date] AS PlannedReceiptDate
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures                                                                                                
  , PSL.[Quantity (Base)] AS ShippedQuantity
  , PSL.[Qty_ Invoiced (Base)] AS InvoicedQuantity
  , PSL.[Qty_ Rcd_ Not Invoiced] AS ReceivedNotInvoicedQuantity
  , PSL.[Gross Weight] AS ShippedGrossWeight
  , PSL.[Net Weight] AS ShippedNetWeight
  , PSL.[Quantity (Base)] * PSL.[Unit Price (LCY)] * ((100 - PSL.[Line Discount _]) / 100) AS ShippedAmount_LCY
  -- measures_RCY
  , PSL.[Quantity (Base)] * PSL.[Unit Price (LCY)] * ((100 - PSL.[Line Discount _]) / 100) * ER.CrossRate AS ShippedAmount_RCY
FROM stage_nav.[Purch_ Rcpt_ Line] AS PSL
LEFT JOIN stage_nav.[Purch_ Rcpt_ Header] AS PSH
  ON PSL.[Buy-From Vendor No_] = PSH.[Buy-From Vendor No_]
    AND PSL.[Document No_] = PSH.[No_]
    AND PSL.Company_Id = PSH.Company_ID
    AND PSL.Data_connection_id = PSH.Data_connection_id
LEFT JOIN help.ExchangeRates AS ER
  ON PSL.[Posting Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = PSL.company_id
    AND ER.DataConnectionID = PSL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON PSL.[Dimension Set ID] = FD.dimensionsetno
    AND PSL.company_id = FD.companyid
    AND PSL.data_connection_id = FD.DataConnectionID
