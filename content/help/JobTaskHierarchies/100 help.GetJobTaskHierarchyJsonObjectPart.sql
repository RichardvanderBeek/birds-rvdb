/****************************************************************************************************
  Functionality: Returns a PART of a json object for a part of the json hierarchy
    {  
        "no"    : "01"
      , "name"  : "job-task-account-01"
      , "level" : 0 
      , "data_connection_id" : 1
      , "company_id" : 2
      , "children" : [

    

  Created by:    JvL	Date:	2019/02/26
  Date 		Changed by 	Ticket/Change 	Description
*****************************************************************************************************/

EXEC dbo.drop_object @object = N'help.GetJobTaskHierarchyJsonObjectPart', @type = N'F' ;
GO
CREATE FUNCTION help.GetJobTaskHierarchyJsonObjectPart
(
  @stage_id           INT
, @jobcode            NVARCHAR(40)
, @code               NVARCHAR(40)
, @name               NVARCHAR(100)
, @level              INT
, @data_connection_id INT
, @company_id         INT
)
RETURNS NVARCHAR(MAX)
BEGIN

  SET @jobcode = REPLACE(@jobcode, '\', '') ;
  SET @jobcode = REPLACE(@jobcode, '"', '\"') ;
  SET @jobcode = REPLACE(@jobcode, CHAR(9), ' ') ;
  SET @jobcode = REPLACE(@jobcode, CHAR(10), ' ') ;
  SET @jobcode = REPLACE(@jobcode, CHAR(13), ' ') ;

  SET @code = REPLACE(@code, '\', '') ;
  SET @code = REPLACE(@code, '"', '\"') ;
  SET @code = REPLACE(@code, CHAR(9), ' ') ;
  SET @code = REPLACE(@code, CHAR(10), ' ') ;
  SET @code = REPLACE(@code, CHAR(13), ' ') ;

  SET @name = REPLACE(@name, '\', '') ;
  SET @name = REPLACE(@name, '"', '\"') ;
  SET @name = REPLACE(@name, CHAR(9), ' ') ;
  SET @name = REPLACE(@name, CHAR(10), ' ') ;
  SET @name = REPLACE(@name, CHAR(13), ' ') ;


  RETURN '{ "stage_id": ' + COALESCE(CAST(@stage_id AS NVARCHAR(40)), 'null') + ', "job_code": "' + COALESCE(CAST(@jobcode AS NVARCHAR(40)), '')
         + '", "code": "' + COALESCE(CAST(@code AS NVARCHAR(40)), '') + '", "name": "' + COALESCE(CAST(@name AS NVARCHAR(100)), '') + '", "level": '
         + COALESCE(CAST(@level AS NVARCHAR(20)), 'null') + ', "data_connection_id": ' + COALESCE(CAST(@data_connection_id AS NVARCHAR(20)), 'null')
         + ', "company_id": '
         + COALESCE(
             CAST(@company_id
AS                                                                                                                                                                                                                                                                                                                                                                                                                                                                NVARCHAR(20))
           , 'null') + ', "children": [ ' ;
END ;
