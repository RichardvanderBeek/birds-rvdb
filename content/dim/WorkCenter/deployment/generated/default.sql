SELECT
  -- System Information
  WC.stage_id AS StageID
  , WC.company_id AS CompanyID
  , WC.data_connection_id AS DataConnectionID
  -- Business Key
  , WC.[No_] AS WorkCenterCode
  -- Attributes
  , COALESCE(ISNULL(WC.NAME, ''), 'N/A') AS WorkCenterName
  , COALESCE(ISNULL(WC.[No_], ''), 'N/A') + ' - ' + COALESCE(ISNULL(WC.NAME, ''), 'N/A') AS WorkCenterCodeName
  , COALESCE(ISNULL(WCG.Code, ''), 'N/A') AS WorkCenterGroupCode
  , COALESCE(ISNULL(WCG.NAME, ''), 'N/A') AS WorkCenterGroupName
  , COALESCE(ISNULL(WCG.Code, ''), 'N/A') + ' - ' + COALESCE(ISNULL(WCG.NAME, ''), 'N/A') AS WorkCenterGroupCodeName
FROM stage_nav.[Work Center] AS WC
LEFT JOIN stage_nav.[Work Center Group] AS WCG
  ON WC.[Work Center Group Code] = WCG.[Code]
    AND WC.company_id = WCG.company_id
    AND WC.data_connection_id = WCG.data_connection_id
