/***********************************************************************************
Functionality:  This SQL script creates the help.SalesItemLedgerEntry
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
DD-MM-YYYY      XXX XXX           DEV-XXXX          XXXX
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesItemLedgerEntry', @type = N'T' ;
GO
CREATE TABLE help.SalesItemLedgerEntry
(
  -- system information
  StageID                BIGINT        NOT NULL
, Company_ID             INT           NOT NULL
, Data_Connection_ID     INT           NOT NULL
, Component_Execution_ID INT           NOT NULL
, Execution_Flag         NVARCHAR(10)  NOT NULL
, EntryNo                INT           NOT NULL
, DocumentType           INT           NOT NULL
, DocumentNo             NVARCHAR(100) NULL
, PostingDate            DATETIME
, DocumentPostingDate    DATETIME
, SelltoCustomerNo       NVARCHAR(100) NULL
, BilltoCustomerNo       NVARCHAR(100) NULL
) ;