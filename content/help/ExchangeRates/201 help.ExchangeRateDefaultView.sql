EXEC dbo.drop_object @object = N'help.ExchangeRatesDefaultView', @type = N'V' ;
GO
CREATE VIEW help.ExchangeRatesDefaultView
AS
  --If there is no exchange rate defined for the reporting currency for a company add 1 as crossrate
  SELECT      dc.data_connection_id     AS DataConnectionId
            , c.company_id              AS CompanyId
            , i.reporting_currency_code AS ToCurrencyCode
            , '1900-01-01'              AS ValidFrom
            , '9999-12-31'              AS ValidTo
            , 1                         AS CrossRate
    FROM      meta.instances        i
   INNER JOIN meta.data_connections dc ON dc.instance_id      = i.instance_id
   INNER JOIN meta.companies        c ON c.data_connection_id = dc.data_connection_id
   WHERE      NOT EXISTS ( SELECT *
                             FROM help.ExchangeRatesView erv
                            WHERE erv.CompanyId        = c.company_id
                              AND erv.DataConnectionId = c.data_connection_id
                              AND erv.ToCurrencyCode   = i.reporting_currency_code) ;
