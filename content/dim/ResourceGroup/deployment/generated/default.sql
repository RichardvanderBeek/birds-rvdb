SELECT
  -- system information
  RG.stage_id AS StageID
  , RG.company_id AS CompanyID
  , RG.data_connection_id AS DataConnectionID
  -- business key
  , RG.No_ AS ResourceGroupCode
  --  attributes                        
  , COALESCE(NULLIF(RG.NAME, ''), 'N/A') AS ResourceGroupName
  , COALESCE(NULLIF(RG.No_, ''), 'N/A') + ' - ' + COALESCE(NULLIF(RG.NAME, ''), 'N/A') AS ResourceGroupCodeName
FROM stage_nav.[Resource Group] AS RG
