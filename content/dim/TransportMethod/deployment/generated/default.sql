SELECT
  -- system information
  TM.stage_id AS StageID
  , TM.company_id AS CompanyID
  , TM.data_connection_id AS DataConnectionID
  -- business key
  , TM.Code AS TransportMethodCode
  --  attributes                        
  , COALESCE(NULLIF(TM.Description, ''), 'N/A') AS TransportMethodDescription
  , COALESCE(NULLIF(TM.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(TM.Description, ''), 'N/A') AS TransportMethodCodeDescription
FROM stage_nav.[Transport Method] AS TM
