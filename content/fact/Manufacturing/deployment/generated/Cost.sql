SELECT
  -- system information
  CLE.stage_id AS StageID
  , CLE.Company_Id AS CompanyID
  , CLE.data_connection_id AS DataConnectionID
  , 2 AS ManufacturingType
  -- dimensions                                                  
  , CLE.[Entry No_] AS EntryNo
  , 5 AS EntryType
  , CLE.[Posting Date] AS PostingDate
  , CLE.[Order No_] AS ProductionOrderCode
  , CLE.[Item No_] AS ItemCode
  , CLE.[Item No_] AS OutputItemCode
  , CLE.No_ AS WorkCenterCode
  , CLE.Type AS SourceType
  , CLE.[Scrap Code] AS ScrapCode
  , CLE.[Stop Code] AS StopCode
  , VE.[Location Code] AS LocationCode
  , CLE.Subcontracting AS SubcontractingCode
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  -- measures
  , CAST(NULL AS DECIMAL(38, 20)) AS ActualOutputQuantity
  , CAST(NULL AS DECIMAL(38, 20)) AS ActualConsumptionQuantity
  , CAST(NULL AS DECIMAL(38, 20)) AS ActualTotalQuantity
  , CAST(NULL AS DECIMAL(38, 20)) ActualOutputCost
  , ISNULL(VE.[Cost Amount (Actual)], 0) AS ActualConsumptionCost
  , ISNULL(VE.[Cost Amount (Actual)], 0) AS ActualTotalCost
  , CASE 
    WHEN VE.[Entry Type] = 3
      THEN VE.[Cost Amount (Actual)]
    ELSE 0
    END AS ActualOverheadCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ScrapQuantity
  , CAST(NULL AS DECIMAL(38, 20)) AS SetupTime
  , CAST(NULL AS DECIMAL(38, 20)) AS RunTime
  , CAST(NULL AS DECIMAL(38, 20)) AS StopTime
FROM stage_nav.[Capacity Ledger Entry] AS CLE
LEFT JOIN stage_nav.[Value Entry] AS VE
  ON CLE.[Entry No_] = VE.[Capacity Ledger Entry No_]
    AND CLE.company_id = VE.company_id
    AND CLE.data_connection_id = VE.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON CLE.[Dimension Set ID] = FD.DimensionSetNo
    AND CLE.company_id = FD.CompanyID
    AND CLE.data_connection_id = FD.DataConnectionID
WHERE CLE.[Entry No_] <> 0
