SELECT
  -- System Information
  JT.stage_id AS StageID
  , JT.company_id AS CompanyID
  , JT.data_connection_id AS DataConnectionID
  -- Business Key                                                                                                 
  , JT.[Job No_] AS JobCode
  -- Attributes                                                                                                                                   
  , COALESCE(JB.Description, 'N/A') AS JobDescription
  , COALESCE(JT.[Job No_], 'N/A') + ' - ' + COALESCE(JB.Description, 'N/A') AS JobCodeDescription
  , COALESCE(JT.[Job Task No_], 'N/A') AS JobTaskCode
  , COALESCE(JT.Description, 'N/A') AS JobTaskDescription
  , COALESCE(JT.[Job Task No_], 'N/A') + ' - ' + COALESCE(JT.Description, 'N/A') AS JobTaskCodeDescription
  , COALESCE(NULLIF(JTH.LevelCode0, ''), 'None') AS LevelCode0
  , COALESCE(NULLIF(JTH.LevelName0, ''), 'None') AS LevelName0
  , COALESCE(NULLIF(JTH.LevelCode0, ''), 'None') + ' - ' + COALESCE(NULLIF(JTH.LevelName0, ''), 'None') AS LevelCodeName0
  , COALESCE(NULLIF(JTH.LevelCode1, ''), 'None') AS LevelCode1
  , COALESCE(NULLIF(JTH.LevelName1, ''), 'None') AS LevelName1
  , COALESCE(NULLIF(JTH.LevelCode1, ''), 'None') + ' - ' + COALESCE(NULLIF(JTH.LevelName1, ''), 'None') AS LevelCodeName1
  , COALESCE(NULLIF(JTH.LevelCode2, ''), 'None') AS LevelCode2
  , COALESCE(NULLIF(JTH.LevelName2, ''), 'None') AS LevelName2
  , COALESCE(NULLIF(JTH.LevelCode2, ''), 'None') + ' - ' + COALESCE(NULLIF(JTH.LevelName2, ''), 'None') AS LevelCodeName2
  , COALESCE(NULLIF(JTH.LevelCode3, ''), 'None') AS LevelCode3
  , COALESCE(NULLIF(JTH.LevelName3, ''), 'None') AS LevelName3
  , COALESCE(NULLIF(JTH.LevelCode3, ''), 'None') + ' - ' + COALESCE(NULLIF(JTH.LevelName3, ''), 'None') AS LevelCodeName3
  , COALESCE(NULLIF(JTH.LevelCode4, ''), 'None') AS LevelCode4
  , COALESCE(NULLIF(JTH.LevelName4, ''), 'None') AS LevelName4
  , COALESCE(NULLIF(JTH.LevelCode4, ''), 'None') + ' - ' + COALESCE(NULLIF(JTH.LevelName4, ''), 'None') AS LevelCodeName4
FROM stage_nav.[Job Task] AS JT
LEFT JOIN stage_nav.Job AS JB
  ON JT.[Job No_] = JB.No_
    AND JT.company_id = JB.company_id
    AND JT.data_connection_id = JB.data_connection_id
LEFT JOIN help.JobTaskHierarchies AS JTH
  ON JT.[Job No_] = JTH.JobCode
    AND JT.[Job Task No_] = JTH.Code
    AND JT.company_id = JTH.CompanyID
    AND JT.data_connection_id = JTH.DataConnectionID
