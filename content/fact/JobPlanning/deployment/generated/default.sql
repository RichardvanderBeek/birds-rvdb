SELECT
  --   system information
  JPL.stage_id AS StageID
  , JPL.company_id AS CompanyID
  , JPL.data_connection_id AS DataConnectionID
  -- dimensions
  , JPL.[Line Type] AS LineType
  , JPL.[Job No_] AS JobCode
  , JPL.[Job Task No_] AS JobTaskCode
  , JPL.[Planning Date] AS PlanningDate
  , CASE 
    WHEN JPL.Type = 0
      THEN JPL.No_
    ELSE NULL
    END AS ResourceCode
  , JPL.[Resource Group No_] AS ResourceGroupCode
  , CASE 
    WHEN JPL.Type = 1
      THEN JPL.No_
    ELSE NULL
    END AS ItemCode
  , CASE 
    WHEN JPL.Type = 2
      THEN JPL.No_
    ELSE NULL
    END AS GeneralLedgerAccountCode
  , JPL.Type AS JobLineTypeCode
  , JB.[Bill-to Customer No_] AS BillToCustomerCode
  , 1003 AS TableID -- Job Planning Line
  --LCY Measures           
  -- Resource          
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 0
      THEN JPL.Quantity
    ELSE 0
    END AS ScheduledHourConsumption -- Schedule, Resource
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 0
      THEN JPL.Quantity
    ELSE 0
    END AS ContractedHourConsumption -- Contract, Resource
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Cost (LCY)]
    ELSE 0
    END AS ScheduledResourceCost_LCY -- Schedule, Resource
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Cost (LCY)]
    ELSE 0
    END AS ContractedResourceCost_LCY -- Contract, Resource
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Price (LCY)]
    ELSE 0
    END AS ScheduledResourceSales_LCY -- Schedule, Resource
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Price (LCY)] * - 1
    ELSE 0
    END AS ContractedResourceSales_LCY -- Contract, Resource
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Line Discount Amount (LCY)]
    ELSE 0
    END AS ScheduledResourceDiscount_LCY -- Schedule, Resource
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Line Discount Amount (LCY)] * - 1
    ELSE 0
    END AS ContractedResourceDiscount_LCY -- Contract, Resource 
  -- Item          
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 1
      THEN JPL.Quantity
    ELSE 0
    END AS ScheduledItemQuantity -- Schedule, Item
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 1
      THEN JPL.Quantity
    ELSE 0
    END AS ContractedItemQuantity -- Contract, Item
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Cost (LCY)]
    ELSE 0
    END AS ScheduledItemCost_LCY -- Schedule, Item
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Cost (LCY)]
    ELSE 0
    END AS ContractedItemCost_LCY -- Contract, Item
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Price (LCY)]
    ELSE 0
    END AS ScheduledItemSales_LCY -- Schedule, Item
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Price (LCY)] * - 1
    ELSE 0
    END AS ContractedItemSales_LCY -- Contract, Item
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Line Discount Amount (LCY)]
    ELSE 0
    END AS ScheduledItemDiscount_LCY -- Schedule, Item
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Line Discount Amount (LCY)] * - 1
    ELSE 0
    END AS ContractedItemDiscount_LCY -- Contract, Item 
  -- G/L Account             
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Cost (LCY)]
    ELSE 0
    END AS ScheduledGLCost_LCY -- Schedule, G/L Account
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Cost (LCY)]
    ELSE 0
    END AS ContractedGLCost_LCY -- Contract, G/L Account
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Price (LCY)]
    ELSE 0
    END AS ScheduledGLSales_LCY -- Schedule, G/L Account
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Price (LCY)]
    ELSE 0
    END AS ContractedGLSales_LCY -- Contract, G/L Account
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Line Discount Amount (LCY)]
    ELSE 0
    END AS ScheduledGLDiscount_LCY -- Schedule, G/L Account
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Line Discount Amount (LCY)]
    ELSE 0
    END AS ContractedGLDiscount_LCY -- Contract, G/L Account 
  --PCY Measures
  -- Resource
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Cost]
    ELSE 0
    END AS ScheduledResourceCost_PCY -- Schedule, Resource
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Cost]
    ELSE 0
    END AS ContractedResourceCost_PCY -- Contract, Resource
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Price]
    ELSE 0
    END AS ScheduledResourceSales_PCY -- Schedule, Resource
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Price] * - 1
    ELSE 0
    END AS ContractedResourceSales_PCY -- Contract, Resource
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Line Discount Amount]
    ELSE 0
    END AS ScheduledResourceDiscount_PCY -- Schedule, Resource
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Line Discount Amount] * - 1
    ELSE 0
    END AS ContractedResourceDiscount_PCY -- Contract, Resource 
  -- Item                
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Cost]
    ELSE 0
    END AS ScheduledItemCost_PCY -- Schedule, Item
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Cost]
    ELSE 0
    END AS ContractedItemCost_PCY -- Contract, Item
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Price]
    ELSE 0
    END AS ScheduledItemSales_PCY -- Schedule, Item
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Price] * - 1
    ELSE 0
    END AS ContractedItemSales_PCY -- Contract, Item
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Line Discount Amount]
    ELSE 0
    END AS ScheduledItemDiscount_PCY -- Schedule, Item
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Line Discount Amount] * - 1
    ELSE 0
    END AS ContractedItemDiscount_PCY -- Contract, Item 
  -- G/L Account                   
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Cost]
    ELSE 0
    END AS ScheduledGLCost_PCY -- Schedule, G/L Account
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Cost]
    ELSE 0
    END AS ContractedGLCost_PCY -- Contract, G/L Account
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Price]
    ELSE 0
    END AS ScheduledGLSales_PCY -- Schedule, G/L Account
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Price]
    ELSE 0
    END AS ContractedGLSales_PCY -- Contract, G/L Account
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Line Discount Amount]
    ELSE 0
    END AS ScheduledGLDiscount_PCY -- Schedule, G/L Account
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Line Discount Amount]
    ELSE 0
    END AS ContractedGLDiscount_PCY -- Contract, G/L Account 
  --RCY Measures
  -- Resource
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Cost] * ER.CrossRate
    ELSE 0
    END AS ScheduledResourceCost_RCY -- Schedule, Resource
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Cost] * ER.CrossRate
    ELSE 0
    END AS ContractedResourceCost_RCY -- Contract, Resource
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Price] * ER.CrossRate
    ELSE 0
    END AS ScheduledResourceSales_RCY -- Schedule, Resource
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Total Price] * - 1 * ER.CrossRate
    ELSE 0
    END AS ContractedResourceSales_RCY -- Contract, Resource
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Line Discount Amount] * ER.CrossRate
    ELSE 0
    END AS ScheduledResourceDiscount_RCY -- Schedule, Resource
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 0
      THEN JPL.[Line Discount Amount] * - 1 * ER.CrossRate
    ELSE 0
    END AS ContractedResourceDiscount_RCY -- Contract, Resource 
  -- Item
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Cost] * ER.CrossRate
    ELSE 0
    END AS ScheduledItemCost_RCY -- Schedule, Item
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Cost] * ER.CrossRate
    ELSE 0
    END AS ContractedItemCost_RCY -- Contract, Item
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Price] * ER.CrossRate
    ELSE 0
    END AS ScheduledItemSales_RCY -- Schedule, Item
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Total Price] * - 1 * ER.CrossRate
    ELSE 0
    END AS ContractedItemSales_RCY -- Contract, Item
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Line Discount Amount] * ER.CrossRate
    ELSE 0
    END AS ScheduledItemDiscount_RCY -- Schedule, Item
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 1
      THEN JPL.[Line Discount Amount] * - 1 * ER.CrossRate
    ELSE 0
    END AS ContractedItemDiscount_RCY -- Contract, Item 
  -- G/L Account   
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Cost] * ER.CrossRate
    ELSE 0
    END AS ScheduledGLCost_RCY -- Schedule, G/L Account
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Cost] * ER.CrossRate
    ELSE 0
    END AS ContractedGLCost_RCY -- Contract, G/L Account
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Price] * ER.CrossRate
    ELSE 0
    END AS ScheduledGLSales_RCY -- Schedule, G/L Account
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Total Price] * ER.CrossRate
    ELSE 0
    END AS ContractedGLSales_RCY -- Contract, G/L Account
  , CASE 
    WHEN JPL.[Schedule Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Line Discount Amount] * ER.CrossRate
    ELSE 0
    END AS ScheduledGLDiscount_RCY -- Schedule, G/L Account
  , CASE 
    WHEN JPL.[Contract Line] = 1
      AND JPL.Type = 2
      THEN JPL.[Line Discount Amount] * ER.CrossRate
    ELSE 0
    END AS ContractedGLDiscount_RCY -- Contract, G/L Account 
FROM stage_nav.[Job Planning Line] AS JPL
LEFT JOIN help.ExchangeRates AS ER
  ON JPL.[Currency Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = JPL.company_id
    AND ER.DataConnectionID = JPL.data_connection_id
LEFT JOIN stage_nav.Job AS JB
  ON JB.No_ = JPL.[Job No_]
    AND JB.company_id = JPL.company_id
    AND JB.data_connection_id = JPL.data_connection_id
