SELECT
  -- system information
  PWSL.stage_id AS StageID
  , PWSL.company_id AS CompanyID
  , PWSL.data_connection_id AS DataConnectionID
, 1 AS WarehouseType
  --dimension
  , PWSL.[No_] AS WarehouseDocumentCode
  , PWSL.[Location Code] AS LocationCode
  , - 1 AS [Status]
  , PWSL.[Source Document] AS SourceDocument
  , PWSL.[Source No_] AS SourceCode
  , PWSL.[Bin Code] AS BinCode
  , PWSL.[Zone Code] AS ZoneCode
  , PWSL.[Item No_] AS ItemCode
  , cast(NULL AS NVARCHAR(20)) AS LotCode
  , PWSL.[Variant Code] AS ItemVariantCode
  , PWSH.[Assigned User ID] AS WarehouseEmployeeCode
  , PWSH.[Posting Date] AS PostingDate
  , CAST('1900-01-01' AS DATETIME) AS WarrantyDate
  , CAST('1900-01-01' AS DATETIME) AS ExpirationDate
  , 2 AS WarehouseDocumentTypeCode -- shipment
  , - 1 AS ActionTypeCode
  , - 1 AS ActivityTypeCode
  -- measures
  , PWSL.[Qty_ (Base)] AS WarehouseQuantityBase
  , CAST(NULL AS DECIMAL(38, 20)) AS WarehouseQuantityPutAwayBase
  , CAST(NULL AS DECIMAL(38, 20)) AS WarehouseCubage
  , CAST(NULL AS DECIMAL(38, 20)) AS WarehouseWeight
FROM stage_nav.[Posted Whse_ Shipment Line] PWSL
INNER JOIN stage_nav.[Posted Whse_ Shipment Header] PWSH
  ON PWSL.[No_] = PWSH.[No_]
    AND PWSL.company_id = PWSH.company_id
    AND PWSL.data_connection_id = PWSH.data_connection_id
