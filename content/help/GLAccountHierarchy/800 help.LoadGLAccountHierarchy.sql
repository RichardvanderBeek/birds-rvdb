EXEC dbo.drop_object @object = N'help.LoadGLAccountHierarchy', @type = N'P' ;
GO
CREATE PROCEDURE help.LoadGLAccountHierarchy
  @component_execution_id INT     = -1
, @load_type              TINYINT = 1
, @inserted               INT     = NULL OUTPUT
, @updated                INT     = NULL OUTPUT
, @deleted                INT     = NULL OUTPUT
AS
BEGIN

  DECLARE @count INT = 0 ;
  -- EXEC @count = help.AddGLAccountHierarchyCSV @component_execution_id = @component_execution_id
  --                                           , @load_type = @load_type
  --                                           , @inserted = @inserted OUTPUT
  --                                           , @updated = @updated OUTPUT
  --                                           , @deleted = @deleted OUTPUT ;


  IF @count = 0
    EXEC help.AddAllGLAccountHierarchy @component_execution_id = @component_execution_id
                                     , @load_type = @load_type
                                     , @inserted = @inserted OUTPUT
                                     , @updated = @updated OUTPUT
                                     , @deleted = @deleted OUTPUT ;

END ;
