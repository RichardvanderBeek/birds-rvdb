SELECT
  -- system information
  SC.stage_id AS StageID
  , SC.company_id AS CompanyID
  , SC.data_connection_id AS DataConnectionID
  -- business key
  , SC.Code AS SourceCode
  --  attributes                        
  , COALESCE(NULLIF(SC.Code, ''), 'N/A') + ' - ' + COALESCE(NULLIF(SC.Description, ''), 'N/A') AS SourceCodeDescription
  , COALESCE(NULLIF(SC.Description, ''), 'N/A') AS SourceDescription
FROM stage_nav.[Source Code] AS SC
