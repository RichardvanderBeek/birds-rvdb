SELECT
  -- system information
  CFF.stage_id AS StageID
  , CFF.company_id AS CompanyID
  , CFF.data_connection_id AS DataConnectionID
  -- business key                                                           
  , CFF.[No_] AS CashflowForecastCode
  -- attributes                                                           
  , CFF.[Description] AS CashflowForecastDescription
  , COALESCE(NULLIF(CFF.[No_], ''), 'N/A') + ' - ' + COALESCE(NULLIF(CFF.[Description], ''), 'N/A') AS CashflowForecastCodeDescription
FROM stage_nav.[Cash Flow Forecast] AS CFF
