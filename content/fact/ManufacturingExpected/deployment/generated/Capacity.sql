SELECT
  -- system information
  PORL.stage_id AS StageID
  , PORL.company_id AS CompanyID
  , PORL.data_connection_id AS DataConnectionID
  , 2 AS ManufacturingExpectedType -- Capacity
  -- dimensions
  , COALESCE(FD.FinancialDimension1ID, 0) AS FinancialDimension1ID
  , COALESCE(FD.FinancialDimension2ID, 0) AS FinancialDimension2ID
  , COALESCE(FD.FinancialDimension3ID, 0) AS FinancialDimension3ID
  , COALESCE(FD.FinancialDimension4ID, 0) AS FinancialDimension4ID
  , COALESCE(FD.FinancialDimension5ID, 0) AS FinancialDimension5ID
  , COALESCE(FD.FinancialDimension6ID, 0) AS FinancialDimension6ID
  , COALESCE(FD.FinancialDimension7ID, 0) AS FinancialDimension7ID
  , COALESCE(FD.FinancialDimension8ID, 0) AS FinancialDimension8ID
  , PORL.[Prod_ Order No_] AS ProductionOrderCode
  , CAST(NULL AS NVARCHAR(20)) AS LocationCode
  , PORL.[No_] AS ItemCode
  , POL.[Variant Code] AS ItemVariantCode
  , POL.[Item No_] AS OutputItemCode
  , POL.STATUS AS StatusCode
  , PORL.[Type] AS SourceType -- Work Center = 0, Machine Center = 1, Item = 3
  , 5 AS EntryType --
  , PORL.[Ending Date] AS [Date]
  -- measures
  , PORL.[Expected Capacity Need] AS ExpectedConsumptionQuantity
  , PORL.[Expected Capacity Need] AS ExpectedTotalQuantity
  , (PORL.[Expected Operation Cost Amt_] - PORL.[Expected Capacity Ovhd_ Cost]) AS ExpectedConsumptionCost
  , (PORL.[Expected Operation Cost Amt_] + PORL.[Expected Capacity Ovhd_ Cost]) AS ExpectedTotalCost
  , PORL.[Expected Capacity Ovhd_ Cost] AS ExpectedOverheadCost
  , PORL.[Expected Capacity Need] AS ExpectedCapacityNeed
  , PORL.[Expected Capacity Ovhd_ Cost] AS ExpectedCapacityOverheadCost
  , PORL.[Run Time] AS ExpectedRunTime
  , PORL.[Setup Time] AS ExpectedSetupTime
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedOutputCost
  , CAST(NULL AS DECIMAL(38, 20)) AS ExpectedOutputQuantity
FROM stage_nav.[Prod_ Order Routing Line] AS PORL
LEFT JOIN stage_nav.[Prod_ Order Line] AS POL
  ON PORL.[Prod_ Order No_] = POL.[Prod_ Order No_]
    AND PORL.[Routing Reference No_] = POL.[Line No_]
    AND PORL.company_id = POL.company_id
    AND PORL.data_connection_id = POL.data_connection_id
LEFT JOIN help.FinancialDimensionsPivotted AS FD
  ON POL.[Dimension Set ID] = FD.DimensionSetNo
    AND POL.company_id = FD.CompanyID
    AND POL.data_connection_id = FD.DataConnectionID
