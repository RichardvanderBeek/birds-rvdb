SELECT 5 AS EntryType -- Consumption
  , 'Consumption' AS EntryTypeDesc
  , a.Type AS SourceType -- Work Center = 0, Machine Center = 1, Item = 3
  , 'Work Center' AS SourceTypeDesc
  , a.No_ AS ConsumptionCode
  , a.No_ + ISNULL(' - ' + NULLIF(b.NAME, ''), '') AS ConsumptionCodeDesc
  , a.[Prod_ Order No_] AS ProductionOrderCode
  , COALESCE(NULLIF(b.[Work Center Group Code], ''), 'N/A') AS ConsumptionGroup1Code
  , COALESCE(NULLIF(b.[Work Center Group Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(c.NAME, ''), 'N/A') AS ConsumptionGroup1CodeDesc
  , COALESCE(NULLIF(b.[Work Center Group Code], ''), 'N/A') AS ConsumptionGroup2Code
  , COALESCE(NULLIF(b.[Work Center Group Code], ''), 'N/A') + ' - ' + COALESCE(NULLIF(c.NAME, ''), 'N/A') AS ConsumptionGroup2CodeDesc
  , 'N/A' AS ProductGroupCode
  , 'N/A' AS ProductGroupCodeDesc
  , a.company_id AS CompanyID
  , a.data_connection_id AS DataConnectionID
  , 5 AS ConsumptionKey
, 'WorkCenterExpected'AS ConsumptionKeyDesc
FROM stage_nav.[Prod_ Order Routing Line] a
LEFT JOIN stage_nav.[Work Center] b
  ON a.No_ = b.No_
    AND a.company_id = b.company_id
    AND a.data_connection_id = b.data_connection_id
LEFT JOIN stage_nav.[Work Center Group] c
  ON b.[Work Center Group Code] = c.Code
    AND b.company_id = c.company_id
    AND b.data_connection_id = c.data_connection_id
WHERE a.Type = 0 -- Work Center
  AND a.No_ <> ''
