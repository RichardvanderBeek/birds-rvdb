SELECT
  --   Business Key                                                                                       
  E_T.OptionNo AS SalesLineTypeCode
  --  Attributes
  , COALESCE(NULLIF(E_T.OptionValue, ''), 'N/A') AS SalesLineTypeDescription
  , E_T.DataConnectionId AS DataConnectionID
FROM help.Enumerations AS E_T
WHERE E_T.TableName = 'Sales Invoice Line'
  AND E_T.ColumnName = 'Type'
