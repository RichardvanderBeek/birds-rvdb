/***********************************************************************************
Functionality:  This SQL script creates the help.SalesInvoiceLineView
Created by:     Thomas van Buren     Date: 04-02-2019  DEV-1667

Date            Changed By        Ticket/Change     Description
30-08-2019      TvB               DEV-2665          Added field Type to correct join to SalesLineType dimension
**************************************************************************************/
EXEC dbo.drop_object @object = N'help.SalesInvoiceLineView', @type = N'V' ;
GO
CREATE VIEW help.SalesInvoiceLineView
AS
  SELECT
    -- system information
                    SIL.stage_id                                     AS StageID
                  , SIL.company_id                                   AS CompanyID
                  , SIL.data_connection_id                           AS DataConnectionID
                  , VE.execution_timestamp                           AS execution_timestamp
                  -- business key
                  , VE.[Entry No_]                                   AS EntryNo
                  , VE.[Document Type]                               AS DocumentType
                  , VE.[Document No_]                                AS DocumentNo
                  , VE.[Posting Date]                                AS PostingDate
                  , COALESCE(SIH.[Posting Date], SIL.[Posting Date]) AS DocumentPostingDate
                  , SIL.[Sell-to Customer No_]                       AS SelltoCustomerNo
                  , SIL.[Bill-to Customer No_]                       AS BilltoCustomerNo
                  , COALESCE(SIH.[Currency Factor], 1)               AS CurrencyFactor
                  , SIH.[Currency Code]                              AS CurrencyCode
                  , SIL.[Type]                                       AS [Type]
    FROM            stage_nav.[Sales Invoice Line]   AS SIL
    LEFT OUTER JOIN stage_nav.[Sales Invoice Header] AS SIH ON SIL.[Document No_]         = SIH.[No_]
                                                           AND SIL.company_id             = SIH.company_id
                                                           AND SIL.data_connection_id     = SIH.data_connection_id
   INNER JOIN       stage_nav.[Value Entry]          AS VE ON VE.[Document Type]          = 2 --Invoices
                                                          AND SIL.[Document No_]          = VE.[Document No_]
                                                          AND SIL.[Line No_]              = VE.[Document Line No_]
                                                          AND VE.[Item Ledger Entry Type] = 1 --Sales
                                                          AND SIL.company_id              = VE.company_id
                                                          AND SIL.data_connection_id      = VE.data_connection_id
   WHERE            SIL.execution_flag <> 'D'
     AND            SIH.execution_flag            <> 'D'
     AND            VE.execution_flag             <> 'D' ;
GO
