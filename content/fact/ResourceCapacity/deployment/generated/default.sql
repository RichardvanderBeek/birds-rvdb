SELECT
  -- system information
  RCE.stage_id AS StageID
  , RCE.Company_Id AS CompanyID
  , RCE.data_connection_id AS DataConnectionID
  -- dimensions
  , RCE.[Resource No_] AS ResourceCode
  , RCE.[Resource Group No_] AS ResourceGroupCode
  , RCE.DATE AS ResourceCapacityDate
  , RCE.[Entry No_] AS EntryCode
  -- measures
  , RCE.Capacity AS ResourceCapacity
FROM stage_nav.[Res_ Capacity Entry] AS RCE
LEFT JOIN help.ExchangeRates AS ER
  ON RCE.[Date] BETWEEN ER.ValidFrom
      AND ER.ValidTo
    AND ER.CompanyId = RCE.company_id
    AND ER.DataConnectionID = RCE.data_connection_id
